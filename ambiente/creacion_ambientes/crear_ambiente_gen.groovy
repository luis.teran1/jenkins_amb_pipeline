properties([
  parameters([
    string(defaultValue: 'master', description: '', name: 'site_branch', trim: false),
    string(defaultValue: 'master', description: '', name: 'nga_branch', trim: false),
    choice(choices: ['dev01', 'dev02', 'dev03', 'dev04', 'dev05', 'dev06', 'dev07'], description: '', name: 'ambiente'),
    choice(choices: ['aws', 'vmware', 'kvm'], description: '', name: 'infra'),
    string(defaultValue: '1.2.8', description: '', name: 'cookbook_base_version', trim: false),
    string(defaultValue: '1.2.0', description: '', name: 'cookbook_asearch_version', trim: false),
    string(defaultValue: '1.2.0', description: '', name: 'cookbook_csearch_version', trim: false),
    string(defaultValue: '1.2.0', description: '', name: 'cookbook_jsearch_version', trim: false),
    string(defaultValue: '1.2.0', description: '', name: 'cookbook_msearch_version', trim: false),
    string(defaultValue: '1.2.0', description: '', name: 'cookbook_bconf_version', trim: false),
    string(defaultValue: '0.3.0', description: '', name: 'cookbook_carga_masiva_common_version', trim: false),
    string(defaultValue: '1.0.1', description: '', name: 'cookbook_dav_version', trim: false),
    string(defaultValue: '0.3.1', description: '', name: 'cookbook_jenkins_carga_masiva_version', trim: false),
    string(defaultValue: '0.4.0', description: '', name: 'cookbook_jenkins_cronjob_version', trim: false),
    string(defaultValue: '0.2.7', description: '', name: 'cookbook_exporters_version', trim: false),
    string(defaultValue: '0.5.0', description: '', name: 'cookbook_hob_carga_masiva_version', trim: false),
    string(defaultValue: '1.3.3', description: '', name: 'cookbook_haproxy_version', trim: false),
    string(defaultValue: '1.2.3', description: '', name: 'cookbook_msite_version', trim: false),
    string(defaultValue: '1.2.33', description: '', name: 'cookbook_nginx_version', trim: false),
    string(defaultValue: '1.3.3', description: '', name: 'cookbook_nga_version', trim: false),
    string(defaultValue: '1.0.0', description: '', name: 'cookbook_redis_version', trim: false),
    string(defaultValue: '0.2.0', description: '', name: 'cookbook_supervisord_version', trim: false),
    string(defaultValue: '1.0.4', description: '', name: 'cookbook_varnish_version', trim: false),
    string(defaultValue: '1.4.0', description: '', name: 'cookbook_trans_version', trim: false),
    string(defaultValue: '1.3.0', description: '', name: 'cookbook_www_version', trim: false),
    string(defaultValue: '1.4.3', description: '', name: 'cookbook_www2_version', trim: false),
    string(defaultValue: 'false', description: '', name: 'no_chain_file', trim: false),
    string(defaultValue: 'false', description: '', name: 'no_ca_file', trim: false),
    string(defaultValue: 'LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUZZakNDQkVxZ0F3SUJBZ0lTQTQxWlNMc2VwMnlhZXYxTklOeitEM2VETUEwR0NTcUdTSWIzRFFFQkN3VUEKTUVveEN6QUpCZ05WQkFZVEFsVlRNUll3RkFZRFZRUUtFdzFNWlhRbmN5QkZibU55ZVhCME1TTXdJUVlEVlFRRApFeHBNWlhRbmN5QkZibU55ZVhCMElFRjFkR2h2Y21sMGVTQllNekFlRncweU1EQXpNalF4T1RRMU1UZGFGdzB5Ck1EQTJNakl4T1RRMU1UZGFNQmd4RmpBVUJnTlZCQU1URFdSbGRqQTJMbmxoY0c4dVkyd3dnZ0VpTUEwR0NTcUcKU0liM0RRRUJBUVVBQTRJQkR3QXdnZ0VLQW9JQkFRRE1wOFBZT2U0NWo0NUVmNloxSytwd1Q0WmRXVC91c1ZmYwpES0c0UmxWYVYzYkdrdmNuS1RXUk11bWxudTFKWXFJTzhhSGZpY2FOS09JTDRyVy92ZDJxVEU1V1o2b1NYQUdjCnEwQ3FGZUM1Q2xQOEtJdFdIaklxZnQxUG1nMGJhV3lrSXllWFYzTFFmL2tyZThuTWN3RUIxSnNyY3RHWmVPSGUKUzdUT3AvRWNuUEhZTDVWMW1KVXI0YmErbTVKNit0MTE0VHg5OGNjK3J5dGRrQkhnaVlTUUtpZk9HTEF3YWNBUwpKVnp2TmU4ejQ2QWZ6enc3NlE5bEpLNmo3eWxieWNNdFB1YWJneHpxUi9aelZrZTRnV1BLUXhEakw2cm9DaXU2CnFkZUprV3k2MkM5WlMxaVJaeFBwNzcrTjZ5eTNaanNPemNnSjBvT0VFS28xMEh0N1ExdXhBZ01CQUFHamdnSnkKTUlJQ2JqQU9CZ05WSFE4QkFmOEVCQU1DQmFBd0hRWURWUjBsQkJZd0ZBWUlLd1lCQlFVSEF3RUdDQ3NHQVFVRgpCd01DTUF3R0ExVWRFd0VCL3dRQ01BQXdIUVlEVlIwT0JCWUVGR3dVZDl3d0pjVlp3REFWM1NlMDA4MmphdHYxCk1COEdBMVVkSXdRWU1CYUFGS2hLYW1NRWZkMjY1dEU1dDZaRlplL3pxT3loTUc4R0NDc0dBUVVGQndFQkJHTXcKWVRBdUJnZ3JCZ0VGQlFjd0FZWWlhSFIwY0RvdkwyOWpjM0F1YVc1MExYZ3pMbXhsZEhObGJtTnllWEIwTG05eQpaekF2QmdnckJnRUZCUWN3QW9ZamFIUjBjRG92TDJObGNuUXVhVzUwTFhnekxteGxkSE5sYm1OeWVYQjBMbTl5Clp5OHdLUVlEVlIwUkJDSXdJSUlQS2k1a1pYWXdOaTU1WVhCdkxtTnNnZzFrWlhZd05pNTVZWEJ2TG1Oc01Fd0cKQTFVZElBUkZNRU13Q0FZR1o0RU1BUUlCTURjR0N5c0dBUVFCZ3Q4VEFRRUJNQ2d3SmdZSUt3WUJCUVVIQWdFVwpHbWgwZEhBNkx5OWpjSE11YkdWMGMyVnVZM0o1Y0hRdWIzSm5NSUlCQXdZS0t3WUJCQUhXZVFJRUFnU0I5QVNCCjhRRHZBSFVBOEpXa1dmSUEwWUpBRUMwdms0aU9yVXYrSFVmam1lSFFOS2F3cUtxT3NuTUFBQUZ4RGtyVzh3QUEKQkFNQVJqQkVBaUJMZlV6dGp3RzJMeWU4NnJEOUhORUV6dmZvTDJCUWtIMEtkaldQV21odkx3SWdLQWwvMFpCdgpoYVhUWUZJQU5na2ZGeExaTk8xWFNzOWFQL0pzenZsdk1uY0FkZ0N5SGdYTWk2TE5paUJPaDJiNUs3bUtKU0JuCmE5cjZjT2V5U1ZNdDc0dVFYZ0FBQVhFT1N0YnNBQUFFQXdCSE1FVUNJRllBaWxCUDNrRElsV2pZMmlnRWV5MWoKN3V4Yy9GMnN3dFJMYVBPUGRib1RBaUVBNkw2RlFleTk3OGxaUFY2N2t6eThBeWxleG51d1JXQTlwVFhOTk9yWgpLTzR3RFFZSktvWklodmNOQVFFTEJRQURnZ0VCQUFTYjM2UUFidHpVazNxZlVNN08wT0RYOSt5dzNhMklvV3lUCi8rMUtabXJuUGhPUk5rMWduSk5BSGRiTTNkRXQ4Smd6TlRHMUtWUjd4MjdIYVM2bkRyWWxDY293bW5JcS9VckcKOWN2THpTTmtJRncxbllQNGxQN2lRaGtOQVVVd2wrNHlMVDhnNTJ4UTVmbDh0dWxDNDZ2SjJvZTU4QU0yQ21TMwp4a1RicXViR1hwUDd4SktUc1lYQ3VldHdtNG5VR3VidGt1R0wzTUdJT1dtMCtjTVhWcFNiV1I5ekdnTEl0K2xYCmZzTjEyUFlPRnFhZlFMdG5XYUF5YkRISmVtWEdOS2VnWkhIUE5HeW9tNlpNWHIzbGF5N1NLaGhHUXA1MzZZMTgKYzRuK3RkdnZQR09qUVNjejc0dmZ0MlBzYkt3cHM5WDc1YmhwVnNVR3ZWQTFBMUNoVHhJPQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCi0tLS0tQkVHSU4gQ0VSVElGSUNBVEUtLS0tLQpNSUlFa2pDQ0EzcWdBd0lCQWdJUUNnRkJRZ0FBQVZPRmMyb0xoZXluQ0RBTkJna3Foa2lHOXcwQkFRc0ZBREEvCk1TUXdJZ1lEVlFRS0V4dEVhV2RwZEdGc0lGTnBaMjVoZEhWeVpTQlVjblZ6ZENCRGJ5NHhGekFWQmdOVkJBTVQKRGtSVFZDQlNiMjkwSUVOQklGZ3pNQjRYRFRFMk1ETXhOekUyTkRBME5sb1hEVEl4TURNeE56RTJOREEwTmxvdwpTakVMTUFrR0ExVUVCaE1DVlZNeEZqQVVCZ05WQkFvVERVeGxkQ2R6SUVWdVkzSjVjSFF4SXpBaEJnTlZCQU1UCkdreGxkQ2R6SUVWdVkzSjVjSFFnUVhWMGFHOXlhWFI1SUZnek1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0MKQVE4QU1JSUJDZ0tDQVFFQW5OTU04RnJsTGtlM2NsMDNnN05vWXpEcTF6VW1HU1hodmI0MThYQ1NMN2U0UzBFRgpxNm1lTlFoWTdMRXF4R2lIQzZQamRlVG04NmRpY2JwNWdXQWYxNUdhbi9QUWVHZHh5R2tPbFpIUC91YVo2V0E4ClNNeCt5azEzRWlTZFJ4dGE2N25zSGpjQUhKeXNlNmNGNnM1SzY3MUI1VGFZdWN2OWJUeVdhTjhqS2tLUURJWjAKWjhoL3BacTRVbUVVRXo5bDZZS0h5OXY2RGxiMmhvbnpoVCtYaHErdzNCcnZhdzJWRm4zRUs2QmxzcGtFTm5XQQphNnhLOHh1UVNYZ3ZvcFpQS2lBbEtRVEdkTURRTWMyUE1UaVZGcnFvTTdoRDhiRWZ3ekIvb25reEV6MHROdmpqCi9QSXphcms1TWNXdnhJME5IV1FXTTZyNmhDbTIxQXZBMkgzRGt3SURBUUFCbzRJQmZUQ0NBWGt3RWdZRFZSMFQKQVFIL0JBZ3dCZ0VCL3dJQkFEQU9CZ05WSFE4QkFmOEVCQU1DQVlZd2Z3WUlLd1lCQlFVSEFRRUVjekJ4TURJRwpDQ3NHQVFVRkJ6QUJoaVpvZEhSd09pOHZhWE55Wnk1MGNuVnpkR2xrTG05amMzQXVhV1JsYm5SeWRYTjBMbU52CmJUQTdCZ2dyQmdFRkJRY3dBb1l2YUhSMGNEb3ZMMkZ3Y0hNdWFXUmxiblJ5ZFhOMExtTnZiUzl5YjI5MGN5OWsKYzNSeWIyOTBZMkY0TXk1d04yTXdId1lEVlIwakJCZ3dGb0FVeEtleHBIc3NjZnJiNFV1UWRmL0VGV0NGaVJBdwpWQVlEVlIwZ0JFMHdTekFJQmdabmdRd0JBZ0V3UHdZTEt3WUJCQUdDM3hNQkFRRXdNREF1QmdnckJnRUZCUWNDCkFSWWlhSFIwY0RvdkwyTndjeTV5YjI5MExYZ3hMbXhsZEhObGJtTnllWEIwTG05eVp6QThCZ05WSFI4RU5UQXoKTURHZ0w2QXRoaXRvZEhSd09pOHZZM0pzTG1sa1pXNTBjblZ6ZEM1amIyMHZSRk5VVWs5UFZFTkJXRE5EVWt3dQpZM0pzTUIwR0ExVWREZ1FXQkJTb1NtcGpCSDNkdXViUk9iZW1SV1h2ODZqc29UQU5CZ2txaGtpRzl3MEJBUXNGCkFBT0NBUUVBM1RQWEVmTmpXRGpkR0JYN0NWVytkbGE1Y0VpbGFVY25lOElrQ0pMeFdoOUtFaWszSkhSUkhHSm8KdU0yVmNHZmw5NlM4VGloUnpadm9yb2VkNnRpNldxRUJtdHp3M1dvZGF0ZytWeU9lcGg0RVlwci8xd1hLdHg4Lwp3QXBJdkpTd3RtVmk0TUZVNWFNcXJTREU2ZWE3M01qMnRjTXlvNWpNZDZqbWVXVUhLOHNvL2pvV1VvSE9VZ3d1Clg0UG8xUVl6KzNkc3prRHFNcDRma2x4QndYUnNXMTBLWHpQTVRaK3NPUEF2ZXl4aW5kbWprVzhsR3krUXNSbEcKUGZaK0c2WjZoN21qZW0wWStpV2xrWWNWNFBJV0wxaXdCaThzYUNiR1M1ak4ycDhNK1grUTdVTktFa1JPYjNONgpLT3FrcW01N1RIMkgzZURKQWtTbmg2L0RORnUwUWc9PQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0t', description: '', name: 'inline_crt', trim: false),
    string(defaultValue: 'LS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tCk1JSUV2QUlCQURBTkJna3Foa2lHOXcwQkFRRUZBQVNDQktZd2dnU2lBZ0VBQW9JQkFRRE1wOFBZT2U0NWo0NUUKZjZaMUsrcHdUNFpkV1QvdXNWZmNES0c0UmxWYVYzYkdrdmNuS1RXUk11bWxudTFKWXFJTzhhSGZpY2FOS09JTAo0clcvdmQycVRFNVdaNm9TWEFHY3EwQ3FGZUM1Q2xQOEtJdFdIaklxZnQxUG1nMGJhV3lrSXllWFYzTFFmL2tyCmU4bk1jd0VCMUpzcmN0R1plT0hlUzdUT3AvRWNuUEhZTDVWMW1KVXI0YmErbTVKNit0MTE0VHg5OGNjK3J5dGQKa0JIZ2lZU1FLaWZPR0xBd2FjQVNKVnp2TmU4ejQ2QWZ6enc3NlE5bEpLNmo3eWxieWNNdFB1YWJneHpxUi9aegpWa2U0Z1dQS1F4RGpMNnJvQ2l1NnFkZUprV3k2MkM5WlMxaVJaeFBwNzcrTjZ5eTNaanNPemNnSjBvT0VFS28xCjBIdDdRMXV4QWdNQkFBRUNnZ0VBYWVZRnVVV0F0OWhJemJrZjhWR2dKaTJBWVoxbEw2WWt5SURnemZIWENNQnAKZmFkZXJLenVRd3dOU3Q1SitzZEhCR1NVVm85OFpsN1pXVDBldnZFSVdxN1JlblI1MUxZM1AxSitTNFMybkFsMApHKzcrNnJrRlRHZ0pMTCtTT0xUOHVwSldVa0cxNzUzSTJXOWNwS1o5eHV5MHZtN1RkbzdPa2JDckJxUndaMVVCCmhkeEJmdmFUU1Y3SUpBMHcwNis2RGxab2ZSd0pndGtSUHlEUm8rQWhiZEJjYzlocVM5ZkMrUmRZWGlVanZZblYKdVRQTXVKaGdJQ1UzRjg3L1RqbzFhN3QrOTd6RDMzT2wvVGMwd2d5OGxmMXRLUlh3U2Zab3RXaExkOUpTWm55RAp4ZG1DcDUwby9CV0s1RlpwMWFRWE83bE83SXArU00yMVNEOWRjcldVZ1FLQmdRRDB1dUZGdFBwczBnY2VYendjCkdJeWhyMFZxRXJxTjd6NGpXSm5ERUtsUlZpK3Q1dlZYcGVGWGJyTXltZEd0SmZpZ0J0SHBCazA0dit5NFRWQXAKN0RhVkxkSU8yUmFXTWNLS0ZLZFNsRlllUkxjdGhXMXlXS2dLTXZtVTBJbEo5RmhTdnYyRzY5aUFjdUlrT0swcwo5dUZaMjhLNUd2SFJNRS9HWTZtNFB3SXh6UUtCZ1FEV0ZISHlpenNvcDBLamRwVmE5Y2dzYzNJcEhWWTNGWkVnCjlhcmZlWkFVcnhCeERjLzFodlJHN3ZwMVRHbC9jOUVnenBOYWU4NUNqNjNLd2hKNUpWZWJKQk54WHcwemVDVzQKMGJDeHJLU01WeWFHK25PVXFIU21xaW9mME1oSS8wYVg2MjR2WGJFOGZlTjJEcUp4cml6NUhha1ZTbHc4aWVVRQp2L0EvRlRmOWRRS0JnQjUrWXE5eTIzbFB1ajlZa243VnVFQTFQS3I0NW5IbEM1TVA2U2piSEozYjVWai8zRFl0Cm55UnIwTHhQV2tiKzRqbmVYNjF2Y1pQT3BrcFlwOUp4Y21Pb0wvSlZybkRNaG5iNkY5YzhVSGE1RFRJSTNCaFIKWDY3VnB5K1p4R0dJcHJDNHg1SldOTVhjaXIwRXF0OWRhN3JzblJpL2VMNGVEUEtsWEhKdXQzRkpBb0dBVmk5RQo4SEE5RU5ERmN5bDZWM1E3Y2luSCsrVmpvQ2wrazlaVFJFU2NUTGR0MXhYSWN3QXh0NXplRi9yN1BlUlNmUEt0ClAvQTN5WFF1YzBKT1RaZEsvWm80ZVp2YXBLM3FBYzNwWko0c1lKTjFYMEhVNWpRclFpMmZaOCtWMGsxekRoWXAKOWNleVlyZ0xxcXVabWtxVXNIWFFiRkhUY0V2VGQyWU9RbGM0ZzBFQ2dZQmZob0JJTGRHclo1am84TzcxaXErNwpWMDh0aUQ4Ui9Ea0JmVmZqZlhYWXY0VUxQbXV2R1JCQXZodmlmekhjTEhPSVpwb1F0Ynl2NFpSK29IOTVBM2RlCmxUN1NqaVNPSVNlVDFZNmR3aXI3STZtbXFXRXQ4bU8yL1VDckpGZ3crZHZLZnhtZ1hiMWJVQ1R0Z1hJVXNOdjUKQzZxaEExbDhCMm5pYkFBcno1WEs2Zz09Ci0tLS0tRU5EIFBSSVZBVEUgS0VZLS0tLS0=', description: '', name: 'inline_key', trim: false),
    booleanParam(defaultValue: false, description: '', name: 'crear_infra'),
    booleanParam(defaultValue: false, description: '', name: 'crear_template')
  ])
])


def repo
def filterd
def controlpanel
def mail
def nga
def bd
def rabbitmq
def logs
def redis
def search
def pagos
def msite
def w2
def ms
def varnish
def dav
def bconf
def trans
def w2apache
def memcached
def nga_redis
def nginx
def www
def cronjobs

node('principal') {
  if (crear_infra == 'true') {
    stage('crear ambiente') {
      def myjob
      if (infra == 'vmware') {
        myjob = build job: 'crear_infra_vmware',
          parameters: [
            string(name: 'ambiente', value: ambiente)
          ],
          propagate: true,
          wait: true
      } else if (infra == 'aws') {
        myjob = build job: 'crear_infra_aws',
          parameters: [
            string(name: 'ambiente', value: ambiente)
          ],
          propagate: true,
          wait: true
      } else if (infra == 'kvm') {
        myjob = build job: 'crear_infra_kvm',
          parameters: [string(name: 'ambiente', value: ambiente)
          ],
          propagate: true,
          wait: true
      }
      variables = myjob.getBuildVariables()
      repo = variables["logs"].replaceAll("\"", "")
      filterd = variables["filterd"].replaceAll("\"", "")
      controlpanel = variables["www"].replaceAll("\"", "")
      mail = "172.21.10.220"
      nga = variables["msite"].replaceAll("\"", "")
      bd = variables["bd"].replaceAll("\"", "")
      rabbitmq = variables["redis"].replaceAll("\"", "")
      logs = variables["logs"].replaceAll("\"", "")
      redis = variables["redis"].replaceAll("\"", "")
      search = variables["filterd"].replaceAll("\"", "")
      pagos = variables["pagos"].replaceAll("\"", "")
      msite = variables["msite"].replaceAll("\"", "")
      w2 = variables["w2"].replaceAll("\"", "")
      ms = variables["ms"].replaceAll("\"", "")
      varnish = variables["varnish"].replaceAll("\"", "")
      dav = variables["dav"].replaceAll("\"", "")
      bconf = variables["bconf"].replaceAll("\"", "")
      trans = variables["trans"].replaceAll("\"", "")
      w2apache = variables["w2"].replaceAll("\"", "")
      memcached = variables["w2"].replaceAll("\"", "")
      nga_redis = variables["redis"].replaceAll("\"", "")
      w2 = variables["w2"].replaceAll("\"", "")
      nginx = variables["nginx"].replaceAll("\"", "")
      www = variables["www"].replaceAll("\"", "")
      cronjobs = variables["cronjobs"].replaceAll("\"", "")
    }
  }
  if (crear_template == 'true') {
    stage('limpiar ambiente') {
      build job: 'borrar_nodos_ambiente',
        parameters: [
          string(name: 'ambiente', value: ambiente)
        ],
        wait: true
    }
    stage('crear template') {
      build job: 'create_ambiente_from_template',
        parameters: [string(name: 'ambiente_nombre', value: ambiente),
                     string(name: 'repo', value: repo),
                     string(name: 'filterd', value: filterd),
                     string(name: 'controlpanel', value: controlpanel),
                     string(name: 'mail', value: mail),
                     string(name: 'nga', value: nga),
                     string(name: 'bd', value: bd),
                     string(name: 'rabbitmq', value: rabbitmq),
                     string(name: 'logs', value: logs),
                     string(name: 'redis', value: redis),
                     string(name: 'search', value: search),
                     string(name: 'pagos', value: pagos),
                     string(name: 'msite', value: msite),
                     string(name: 'w2', value: w2),
                     string(name: 'ms', value: ms),
                     string(name: 'varnish', value: varnish),
                     string(name: 'dav', value: dav),
                     string(name: 'bconf', value: bconf),
                     string(name: 'trans', value: trans),
                     string(name: 'w2apache', value: trans),
                     string(name: 'memcached', value: trans),
                     string(name: 'redis_nga', value: nga_redis),
                     string(name: 'w2', value: trans),
                     string(name: 'nginx', value: nginx),
                     string(name: 'inline_crt', value: inline_crt),
                     string(name: 'inline_key', value: inline_key),
                     string(name: 'balanced_ip_msite', value: '172.21.8.227'),
                     string(name: 'balanced_ip_dav', value: '172.21.8.226'),
                     string(name: 'balanced_ip_varnish', value: '172.21.8.225'),
                     string(name: 'balanced_ip_nga01', value: '172.21.8.222'),
                     string(name: 'balanced_ip_trans', value: '172.21.8.221'),
                     string(name: 'balanced_ip_w2apache', value: '172.21.8.220'),
                     string(name: 'balanced_ip_search', value: '172.21.8.194'),
                     string(name: 'balanced_ip_nginx', value: '172.21.8.193'),
                     string(name: 'balanced_ip_nga02', value: '172.21.8.192'),
                     string(name: 'balanced_ip_www', value: '172.21.8.228'),
                     string(name: 'virtual_router_id', value: '64'),
                     string(name: 'cookbook_base_version', value: cookbook_base_version),
                     string(name: 'cookbook_asearch_version', value: cookbook_asearch_version),
                     string(name: 'cookbook_csearch_version', value: cookbook_csearch_version),
                     string(name: 'cookbook_jsearch_version', value: cookbook_jsearch_version),
                     string(name: 'cookbook_msearch_version', value: cookbook_msearch_version),
                     string(name: 'cookbook_bconf_version', value: cookbook_bconf_version),
                     string(name: 'cookbook_carga_masiva_common_version', value: cookbook_carga_masiva_common_version),
                     string(name: 'cookbook_dav_version', value: cookbook_dav_version),
                     string(name: 'cookbook_jenkins_carga_masiva_version', value: cookbook_jenkins_carga_masiva_version),
                     string(name: 'cookbook_jenkins_cronjob_version', value: cookbook_jenkins_cronjob_version),
                     string(name: 'cookbook_exporters_version', value: cookbook_exporters_version),
                     string(name: 'cookbook_hob_carga_masiva_version', value: cookbook_hob_carga_masiva_version),
                     string(name: 'cookbook_haproxy_version', value: cookbook_haproxy_version),
                     string(name: 'cookbook_msite_version', value: cookbook_msite_version),
                     string(name: 'cookbook_nginx_version', value: cookbook_nginx_version),
                     string(name: 'cookbook_nga_version', value: cookbook_nga_version),
                     string(name: 'cookbook_redis_version', value: cookbook_redis_version),
                     string(name: 'cookbook_supervisord_version', value: cookbook_supervisord_version),
                     string(name: 'cookbook_varnish_version', value: cookbook_varnish_version),
                     string(name: 'cookbook_trans_version', value: cookbook_trans_version),
                     string(name: 'cookbook_www_version', value: cookbook_www_version),
                     string(name: 'cookbook_www2_version', value: cookbook_www2_version),
                     string(name: 'no_chain_file', value: no_chain_file),
                     string(name: 'no_ca_file', value: no_ca_file),
                     string(name: 'www', value: www)
        ],
        propagate: true,
        wait: true
    }
  }
  stage('Crear Namespace') {
    build job: 'terraform_k8s_namespace', parameters: [
      string(name: 'ambiente', value: ambiente)
    ], wait: true
  }
  stage('Bootstrap Repo') {
    build job: 'bootstrap_server', parameters: [
      string(name: 'ambiente', value: ambiente),
      string(name: 'infra', value: infra),
      string(name: 'ip', value: repo),
      string(name: 'nombre_chef', value: 'repo01'),
      string(name: 'receta', value: 'recipe[base::yapo_repo],role[repo],role[logs]')
    ], wait: true
  }
  stage('compilar') {
    parallel(
      paquetes_sitio: {
        retry(count: 5) {
          build job: 'compilar_sitio', parameters: [string(name: 'site_branch', value: site_branch), string(name: 'ip_repo', value: repo), string(name: 'ambiente_nombre', value: ambiente)], wait: true
        }

      },
      paquetes_nga: {
        retry(count: 5) {
          build job: 'compilar_nga', parameters: [string(name: 'nga_branch', value: nga_branch), string(name: 'ip_repo', value: repo), string(name: 'ambiente_nombre', value: ambiente)], wait: true
        }
      }
    )
  }
  stage('postgres') {
    parallel(
      create_postgres: {
        def roles_postgres = 'role[postgres],recipe[base::ssh_keys]'
        if (infra == 'vmware' || infra == 'kvm') {
          roles_postgres = 'role[base],recipe[postgres::install_centos7],recipe[postgres::lvm_postgres],recipe[aws::default],recipe[postgres::restore_pg_base],role[postgres]'
        } else if (infra == 'kvm') {
          roles_postgres = 'role[base],recipe[postgres::install_centos7],recipe[aws::default],recipe[postgres::restore_pg_base],role[postgres]'
        }
        build job: 'bootstrap_server', parameters: [
          string(name: 'ambiente', value: ambiente),
          string(name: 'infra', value: infra),
          string(name: 'ip', value: bd),
          string(name: 'centos', value: '7'),
          string(name: 'nombre_chef', value: 'bd01'),
          string(name: 'receta', value: roles_postgres)
        ], wait: true

        if (infra == 'vmware') {
          build job: 'send_command', parameters: [
            string(name: 'ambiente', value: ambiente),
            string(name: 'comando', value: 'yum remove blocket* -y'),
            string(name: 'role', value: 'postgres')
          ], wait: true
          build job: 'send_command', parameters: [
            string(name: 'ambiente', value: ambiente),
            string(name: 'comando', value: 'chef-client'),
            string(name: 'role', value: 'postgres')
          ], wait: true


        }

      }

    )

  }
  stage('bconf') {
    build job: 'bootstrap_server', parameters: [
      string(name: 'ambiente', value: ambiente),
      string(name: 'infra', value: infra),
      string(name: 'ip', value: bconf),
      string(name: 'nombre_chef', value: 'bconf01'),
      string(name: 'receta', value: 'recipe[base::yapo_repo],role[bconf]')
    ], wait: true
  }
  stage('persistencia') {
    parallel(
      create_redis: {
        roles_redis = 'recipe[base::yapo_repo],role[redisdev],role[rabbitmq]'
        if (infra == 'vmware') {
          roles_redis = 'recipe[redis::lvm_redis],recipe[base::yapo_repo],role[redisdev],role[rabbitmq]'
        }
        build job: 'bootstrap_server', parameters: [
          string(name: 'ambiente', value: ambiente),
          string(name: 'infra', value: infra),
          string(name: 'ip', value: redis),
          string(name: 'nombre_chef', value: 'redis01'),
          string(name: 'receta', value: roles_redis)
        ], wait: true
      },
      create_search: {
        build job: 'bootstrap_server', parameters: [
          string(name: 'ambiente', value: ambiente),
          string(name: 'infra', value: infra),
          string(name: 'ip', value: search),
          string(name: 'nombre_chef', value: 'search01'),
          string(name: 'receta', value: 'recipe[base::yapo_repo],role[searchdev],role[filterd],role[dav],role[postman]')
        ], wait: true
      }
    )
  }
  stage('aplicacion') {
    parallel(
      create_w2trans: {
        build job: 'bootstrap_server', parameters: [
          string(name: 'ambiente', value: ambiente),
          string(name: 'infra', value: infra),
          string(name: 'ip', value: trans),
          string(name: 'nombre_chef', value: 'trans01'),
          string(name: 'receta', value: 'recipe[base::yapo_repo],role[w2trans],recipe[memcached::default]')
        ], wait: true
      },
      create_www: {
        build job: 'bootstrap_server', parameters: [
          string(name: 'ambiente', value: ambiente),
          string(name: 'infra', value: infra),
          string(name: 'ip', value: www),
          string(name: 'nombre_chef', value: 'www01'),
          string(name: 'receta', value: 'recipe[base::yapo_repo],role[www],role[trans],role[controlpanel]')
        ], wait: true
      },
      create_nga: {
        build job: 'bootstrap_server', parameters: [
          string(name: 'ambiente', value: ambiente),
          string(name: 'infra', value: infra),
          string(name: 'ip', value: nga),
          string(name: 'nombre_chef', value: 'nga01'),
          string(name: 'receta', value: 'recipe[base::yapo_repo],role[nga],role[msite]')
        ], wait: true
      },
      create_varnish: {
        build job: 'bootstrap_server', parameters: [
          string(name: 'ambiente', value: ambiente),
          string(name: 'infra', value: infra),
          string(name: 'ip', value: varnish),
          string(name: 'centos', value: '7'),
          string(name: 'nombre_chef', value: 'varnish01'),
          string(name: 'receta', value: 'recipe[base::yapo_repo],role[varnish]')
        ], wait: true
      },
      create_pagos: {
        build job: 'bootstrap_server', parameters: [
          string(name: 'ambiente', value: ambiente),
          string(name: 'infra', value: infra),
          string(name: 'ip', value: pagos),
          string(name: 'nombre_chef', value: 'pagos01'),
          string(name: 'receta', value: 'recipe[base::yapo_repo],role[pagos]')
        ], wait: true
      },
      create_nginx: {
        build job: 'bootstrap_server', parameters: [
          string(name: 'ambiente', value: ambiente),
          string(name: 'infra', value: infra),
          string(name: 'ip', value: nginx),
          string(name: 'centos', value: '7'),
          string(name: 'nombre_chef', value: 'nginx01'),
          string(name: 'receta', value: 'recipe[base::yapo_repo],role[nginx]')
        ], wait: true
      },
      create_cron_jobs: {
        build job: 'bootstrap_server', parameters: [
          string(name: 'ambiente', value: ambiente),
          string(name: 'infra', value: infra),
          string(name: 'ip', value: cronjobs),
          string(name: 'nombre_chef', value: 'cronjobs01'),
          string(name: 'receta', value: 'recipe[base::yapo_repo],role[jenkins_cronjob]')
        ], wait: true
      }
    )
  }
  stage("habilitar search , msite, cambio de nombre y creacion de MS") {
    parallel(
      crear_indices_search: {
        build job: 'crear_indice',
          parameters: [
            string(name: 'ambiente', value: ambiente),
            string(name: 'tipo_search', value: 'asearch'),
            string(name: 'search', value: search)
          ], wait: true
        build job: 'crear_indice',
          parameters: [
            string(name: 'ambiente', value: ambiente),
            string(name: 'tipo_search', value: 'csearch'),
            string(name: 'search', value: search)
          ], wait: true
        build job: 'crear_indice',
          parameters: [
            string(name: 'ambiente', value: ambiente),
            string(name: 'tipo_search', value: 'josesearch'),
            string(name: 'search', value: search)
          ], wait: true
        build job: 'crear_indice',
          parameters: [
            string(name: 'ambiente', value: ambiente),
            string(name: 'tipo_search', value: 'msearch'),
            string(name: 'search', value: search)
          ], wait: true
      },
      Add_api_key_para_msite: {
        build job: 'enable_msite_pipeline',
          parameters: [
            string(name: 'ambiente', value: ambiente)
          ],
          wait: true
      },
      cambio_de_dominio: {
        stage('Cambiar nombre ambiente en conf') {
          build job: 'cambiar_nombre_dominio',
            parameters: [
              string(name: 'ambiente_nombre', value: ambiente)
            ],
            wait: true
        }
      }
    )
  }
}