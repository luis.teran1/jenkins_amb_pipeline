properties([
  parameters([
    choice(choices: ['dev01', 'dev02', 'dev03', 'dev04', 'dev05', 'dev06'], description: '', name: 'ambiente'),
    booleanParam(defaultValue: false, description: '', name: 'only_destroy')
  ])
])

environment {
  def filterd = ""
  def controlpanel = ""
  def nga = ""
  def www = ""
  def bd = ""
  def rabbitmq = ""
  def logs = ""
  def redis = ""
  def search = ""
  def pagos = ""
  def msite = ""
  def w2 = ""
  def dav = ""
  def bconf = ""
  def trans = ""
  def varnish = ""
  def cronjobs = ""
  def nginx = ""
  def ms = ""
  def repo = ""
}


node('docker_host_jenkins_prod') {


  stage('crear ambiente vmware') {
    deleteDir()
    git branch: 'master',
      credentialsId: '71c4a739-452c-4971-a55e-718de21816d1',
      url: 'git@github.mpi-internal.com:Yapo/cloudformation.git'
    sh 'ls'

    dir('vmware') {
      dir('persistencia') {
        retry(count: 5) {
          sh label: '', script: 'sudo sed -i \'s/{ambiente}/' + ambiente + '/g\' infra_vmware_persistencia.tf'
          sh 'terraform init'
          sh 'terraform destroy -auto-approve'
          if (only_destroy == 'false') {

            sh 'terraform apply -auto-approve'
            env.filterd = sh(returnStdout: true, script: 'terraform output vmware_search')
            env.filterd = env.filterd.replaceAll(~/\n/, "")

            env.bd = sh(returnStdout: true, script: 'terraform output vmware_bd')
            env.bd = env.bd.replaceAll(~/\n/, "")

            env.rabbitmq = sh(returnStdout: true, script: 'terraform output vmware_redis')
            env.rabbitmq = rabbitmq.replaceAll(~/\n/, "")


            env.redis = sh(returnStdout: true, script: 'terraform output vmware_redis')
            env.redis = env.redis.replaceAll(~/\n/, "")

            env.search = sh(returnStdout: true, script: 'terraform output vmware_search')
            env.search = env.search.replaceAll(~/\n/, "")

            env.dav = sh(returnStdout: true, script: 'terraform output vmware_search')
            env.dav = env.dav.replaceAll(~/\n/, "")

            env.varnish = sh(returnStdout: true, script: 'terraform output vmware_varnish')
            env.varnish = env.varnish.replaceAll(~/\n/, "")
          }
        }
      }
      dir('blocket') {
        retry(count: 5) {
          sh label: '', script: 'sudo sed -i \'s/{ambiente}/' + ambiente + '/g\' infra_vmware_blocket.tf'
          sh 'terraform init'
          sh 'terraform destroy -auto-approve'
          if (only_destroy == 'false') {

            sh 'terraform apply -auto-approve'

            env.controlpanel = sh(returnStdout: true, script: 'terraform output vmware_www')
            env.controlpanel = env.controlpanel.replaceAll(~/\n/, "")

            env.nga = sh(returnStdout: true, script: 'terraform output vmware_msite')
            env.nga = env.nga.replaceAll(~/\n/, "")

            env.www = sh(returnStdout: true, script: 'terraform output vmware_www')
            env.www = env.www.replaceAll(~/\n/, "")


            env.msite = sh(returnStdout: true, script: 'terraform output vmware_msite')
            env.msite = env.msite.replaceAll(~/\n/, "")

            env.w2 = sh(returnStdout: true, script: 'terraform output vmware_w2trans')
            env.w2 = env.w2.replaceAll(~/\n/, "")

            env.bconf = sh(returnStdout: true, script: 'terraform output vmware_bconf')
            env.bconf = env.bconf.replaceAll(~/\n/, "")

            env.trans = sh(returnStdout: true, script: 'terraform output vmware_w2trans')
            env.trans = env.trans.replaceAll(~/\n/, "")


            env.cronjobs = sh(returnStdout: true, script: 'terraform output vmware_cronjobs')
            env.cronjobs = env.cronjobs.replaceAll(~/\n/, "")


            env.ms = sh(returnStdout: true, script: 'terraform output instance_rancher_ip')
            env.ms = env.ms.replaceAll(~/\n/, "")

          }
        }
      }
      dir('otros') {
        retry(count: 5) {
          sh label: '', script: 'sudo sed -i \'s/{ambiente}/' + ambiente + '/g\' infra_vmware_otros.tf'
          sh 'terraform init'
          sh 'terraform destroy -auto-approve'
          if (only_destroy == 'false') {

            sh 'terraform apply -auto-approve'
            env.logs = sh(returnStdout: true, script: 'terraform output vmware_logs')
            env.logs = env.logs.replaceAll(~/\n/, "")

            env.pagos = sh(returnStdout: true, script: 'terraform output vmware_pagos')
            env.pagos = env.pagos.replaceAll(~/\n/, "")

            env.nginx = sh(returnStdout: true, script: 'terraform output vmware_nginx')
            env.nginx = env.nginx.replaceAll(~/\n/, "")

            env.repo = sh(returnStdout: true, script: 'terraform output vmware_logs')
            env.repo = env.repo.replaceAll(~/\n/, "")


          }
        }
      }

    }
  }
}
if (only_destroy == 'false') {
  node('terraform_nodo') {
    stage('lvm redis') {
      deleteDir()
      git branch: 'master',
        credentialsId: '71c4a739-452c-4971-a55e-718de21816d1',
        url: 'git@github.mpi-internal.com:Yapo/cloudformation.git'
      sh 'ls'

      dir('vmware') {
        dir('commandos') {
          retry(count: 5) {
            sh 'terraform init'
            sh 'terraform apply -var="ip_grow=' + env.redis + '" -auto-approve'

          }
        }
      }

    }
  }
}

    