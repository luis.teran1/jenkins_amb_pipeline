CLUSTER_INDEX = cluster_index.padLeft(2, '0')
KVM_NODE = kvm_node
VIRT_INSTALL_DELTA = 6
CONTEXT_CLASS = "172.21.1${CLUSTER_INDEX}"
CONTEXT_CLUSTER = "k8s${CLUSTER_INDEX}"
CONTEXT_DOMAIN = "-${CONTEXT_CLUSTER}.dev.yapo.cl"
ENVIRONMENT = "infra-dev-${CONTEXT_CLUSTER}"
GATEWAY = "172.21.0.1"
NETMASK = "255.255.0.0"
DNS = "172.21.10.99"
KS = "http://172.21.10.101:8081/ks/centos7-static.txt"
CLUSTER_NETWORK = "10.2${CLUSTER_INDEX}.0.0/16"
KUBECTL_BASE_COMMAND = 'export KUBECONFIG=/etc/kubernetes/admin.conf ; '
KUBERNETS_VERSION = "1.18.2"
KUBEADM_INIT_COMMAND = "kubeadm init --control-plane-endpoint %s --pod-network-cidr ${CLUSTER_NETWORK} --upload-certs --kubernetes-version ${KUBERNETS_VERSION}"
SSH_COMMAND = 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i %s %s@%s "%s"'
KUBEADM_NETWORK_COMMAND = "${KUBECTL_BASE_COMMAND} kubectl apply -f 'https://cloud.weave.works/k8s/net?k8s-version=%s&env.IPALLOC_RANGE=\'${CLUSTER_NETWORK}\''"
SSH_VERSION_COMMAND = "${KUBECTL_BASE_COMMAND} kubectl version |base64 |tr -d '\n'"
KUBEADM_JOIN_COMMAND = "${KUBECTL_BASE_COMMAND} kubeadm token create --print-join-command"
ROOK_RELEASE = "release-1.4"

CREDENTIAL_GIT_SRV_YAP_DEVOPS = '9c4e41a9-6dbf-426e-bd2a-f1d02d2fcd69'

CHEF_SERVER_URL = "https://chef-server.pro.yapo.cl/organizations/yapo"

CHEF_CREDENTIAL = '9289af24-e2f2-4b4d-a4ae-5ab7dbabeb8a'

CREDENTIAL_JENKINS_SSH = '0465c64d-ce73-4808-be49-8133be8e5304'

HOSTS = [
  apiservers: [
    [
      hostname  : 'apiserver',
      os_size   : APISERVER_OS_SIZE,
      memory    : APISERVER_MEMORY,
      cpus      : APISERVER_CPUS,
      last_octet: "11",
      run_list  : 'role[k8s_apiserver]'
    ]
  ],
  masters   : [
    [
      hostname  : 'master01',
      os_size   : MASTER_OS_SIZE,
      memory    : MASTER_MEMORY,
      cpus      : MASTER_CPUS,
      last_octet: "21",
      run_list  : 'role[k8s_master]'

    ]
  ],
  nodes     : [
    [
      hostname  : 'node01',
      os_size   : NODE_OS_SIZE,
      rook_size : NODE_ROOK_SIZE,
      memory    : NODE_MEMORY,
      cpus      : NODE_CPUS,
      last_octet: "31",
      run_list  : 'role[k8s_node]'
    ],
    [
      hostname  : 'node02',
      os_size   : NODE_OS_SIZE,
      rook_size : NODE_ROOK_SIZE,
      memory    : NODE_MEMORY,
      cpus      : NODE_CPUS,
      last_octet: "32",
      run_list  : 'role[k8s_node]'
    ],
    [
      hostname  : 'node03',
      os_size   : NODE_OS_SIZE,
      rook_size : NODE_ROOK_SIZE,
      memory    : NODE_MEMORY,
      cpus      : NODE_CPUS,
      last_octet: "33",
      run_list  : 'role[k8s_node]'
    ]
  ]
]

for (HOST_KEY in HOSTS.keySet()) {
  for (int i = 0; i < HOSTS[HOST_KEY].size(); i++) {
    def disks = []
    def fqdn = HOSTS[HOST_KEY][i]['hostname'] + CONTEXT_DOMAIN
    HOSTS[HOST_KEY][i]['fqdn'] = fqdn
    HOSTS[HOST_KEY][i]['ip'] = CONTEXT_CLASS + "." + HOSTS[HOST_KEY][i]['last_octet']

    def DISK_OPTION = "--disk path=/vms/%s,size=%s,format=raw,bus=virtio"
    disks << String.format(DISK_OPTION, fqdn, HOSTS[HOST_KEY][i]['os_size'])
    if (HOSTS[HOST_KEY][i]['rook_size']) {
      disks << String.format(DISK_OPTION, String.format("%s-rook", fqdn), HOSTS[HOST_KEY][i]['rook_size'])
    }
    HOSTS[HOST_KEY][i]['disks'] = disks
  }
}

def master = HOSTS['masters'][0]

/*
if (false) {
  node('docker_all') {
    withCredentials(bindings: [
      sshUserPrivateKey(
        credentialsId: CREDENTIAL_JENKINS_SSH,
        keyFileVariable: 'SSH_KEY_PATH',
        usernameVariable: 'SSH_KEY_USER'
      ),
      sshUserPrivateKey(
        credentialsId: CHEF_CREDENTIAL,
        keyFileVariable: 'KNIFE_KEY_PATH',
        usernameVariable: 'KNIFE_NODE_NAME'
      )
    ]) {

      def version_base64 = sh(returnStdout: true, script: String.format(SSH_COMMAND, env.SSH_KEY_PATH, env.SSH_KEY_USER, master['ip'], SSH_VERSION_COMMAND))
      println(version_base64)
      def KNIFE_COMMON_OPTIONS = "--config-option client_key=${KNIFE_KEY_PATH} --config-option node_name=${KNIFE_NODE_NAME} --config-option chef_server_url=${CHEF_SERVER_URL}"
      def KNIFE_SSH_OPTIONS = "-x ${SSH_KEY_USER} -i ${SSH_KEY_PATH}"
      def node_list = sh(returnStdout: true, script: "knife node list ${KNIFE_COMMON_OPTIONS}")
      println(node_list)

      def knife_ssh_output = sh(returnStdout: true, script: "knife ssh ${KNIFE_COMMON_OPTIONS} 'name:master01-k8s01.dev.yapo.cl' -a ipaddress ${KNIFE_SSH_OPTIONS} 'hostname'")
      println(knife_ssh_output)
    }
  }
}
*/

// choices to be set
zap = 'false'
prometheus_central_init = 'false'
nodeport_proxy_init = 'false'

if (zap != 'false') {
  node(KVM_NODE) {
    stage('kubeadm reset') {
      parallel(
        master01: {
        },
        node01: {
        },
        node02: {
        },
        node03: {
        }
      )
    }
    stage('rook zap') {
    }
    stage('docker_system_prune') {
    }
  }
}

if (virt_destroy != 'false') {
  node(KVM_NODE) {
    stage('virt_destroy') {
      def VIRT_DESTROY_COMMAND = 'virsh destroy %s && virsh undefine --domain %s --remove-all-storage || true'
      for (HOST_KEY in HOSTS.keySet()) {
        for (int i = 0; i < HOSTS[HOST_KEY].size(); i++) {
          def fqdn = HOSTS[HOST_KEY][i]['fqdn']
          stage(fqdn) {
            def virt_destroy_command = String.format(VIRT_DESTROY_COMMAND, fqdn, fqdn)
            sh virt_destroy_command
          }
        }
      }
    }
  }
}
if (virt_install != 'false') {
  def VIRT_INSTALL_COMMAND = 'sleep %s && virt-install --name %s --memory %s --vcpus=%s --location=/var/lib/libvirt/images/CentOS-7-x86_64-Minimal-2003.iso %s --network bridge=br0,model=virtio --extra-args "text ip=%s::%s:%s:%s:eth0:none nameserver=%s inst.ks=%s"'
  def virt_install_steps = [:]
  def j = 0
  for (HOST_KEY in HOSTS.keySet()) {
    for (int i = 0; i < HOSTS[HOST_KEY].size(); i++) {
      int index = i
      def fqdn = HOSTS[HOST_KEY][index]['fqdn']
      def memory = HOSTS[HOST_KEY][index]['memory']
      def cpus = HOSTS[HOST_KEY][index]['cpus']
      def disks = HOSTS[HOST_KEY][index]['disks']
      def ip = HOSTS[HOST_KEY][index]['ip']
      def virt_install_command = String.format(VIRT_INSTALL_COMMAND, VIRT_INSTALL_DELTA * j, fqdn, memory, cpus, disks.join(' '), ip, GATEWAY, NETMASK, fqdn, DNS, KS)
      j += 1
      stage("virt_install:${fqdn}") {
        virt_install_steps["step:${fqdn}"] = {
          node(KVM_NODE) {
            sh virt_install_command
          }
        }
      }
    }
  }
  parallel virt_install_steps
}
if (create_env != 'false') {
  node('docker_all') {
    stage('chef init ' + CONTEXT_CLUSTER) {
      stage('create dir') {
        sh 'rm -rf chef'
        sh 'mkdir chef'
        sh 'pwd'
      }
      stage('download repo') {
        dir('chef') {
          sh 'rm -rf *'
          git branch: 'master',
            credentialsId: CREDENTIAL_GIT_SRV_YAP_DEVOPS,
            url: 'git@github.mpi-internal.com:Yapo/chef.git'

          sh "cp environments/infra-dev-k8s0x.json environments/${ENVIRONMENT}.json"
          fecha = sh(returnStdout: true, script: 'date +"%Y-%m-%d %H:%M"').trim()
          sh 'sed "s/serial: YYYY-MM-DD HH:MM/serial: ' + fecha + '/g" -i environments/' + ENVIRONMENT + '.json'
          sh 'sed "s/k8s0x/' + CONTEXT_CLUSTER + '/g"' + " -i environments/${ENVIRONMENT}.json"

          withCredentials(bindings: [
            sshUserPrivateKey(
              credentialsId: CHEF_CREDENTIAL,
              keyFileVariable: 'KNIFE_KEY_PATH',
              usernameVariable: 'KNIFE_NODE_NAME'
            )
          ]) {
            def KNIFE_COMMON_OPTIONS = "--config-option client_key=${KNIFE_KEY_PATH} --config-option node_name=${KNIFE_NODE_NAME} --config-option chef_server_url=${CHEF_SERVER_URL}"

            sh "knife upload ${KNIFE_COMMON_OPTIONS} environments/${ENVIRONMENT}.json --chef-repo-path `pwd`"
          }

          sshagent([CREDENTIAL_GIT_SRV_YAP_DEVOPS]) {
            sh 'git config --global user.email "jenkins.yapo@adevinta.com" && git config --global user.name "Jenkins Yapo"'
            sh "git add environments/${ENVIRONMENT}.json"
            sh "git commit environments/${ENVIRONMENT}.json -m 'add: ${ENVIRONMENT}'"
            sh 'git branch --set-upstream-to=origin/master master'
            sh 'git fetch && git pull --rebase && git push'
          }
        }
      }
    }
  }
}
if (bootstrap != 'false') {
  def bootstrap_steps = [:]
  for (HOST_KEY in HOSTS.keySet()) {
    for (int i = 0; i < HOSTS[HOST_KEY].size(); i++) {
      int index = i
      def fqdn = HOSTS[HOST_KEY][index]['fqdn']
      def ip = HOSTS[HOST_KEY][index]['ip']
      def run_list = HOSTS[HOST_KEY][index]['run_list']
      stage("virt_install:${fqdn}") {
        bootstrap_steps["step:${fqdn}"] = {
          node('docker_all') {
            withCredentials(bindings: [
              sshUserPrivateKey(
                credentialsId: CREDENTIAL_JENKINS_SSH,
                keyFileVariable: 'SSH_KEY_PATH',
                usernameVariable: 'SSH_KEY_USER'
              ),
              sshUserPrivateKey(
                credentialsId: CHEF_CREDENTIAL,
                keyFileVariable: 'KNIFE_KEY_PATH',
                usernameVariable: 'KNIFE_NODE_NAME'
              )]
            ) {

              KNIFE_COMMON_OPTIONS = ""
              KNIFE_COMMON_OPTIONS += " --config-option client_key=${KNIFE_KEY_PATH}"
              KNIFE_COMMON_OPTIONS += " --config-option node_name=${KNIFE_NODE_NAME}"
              KNIFE_COMMON_OPTIONS += " --config-option chef_server_url=${CHEF_SERVER_URL}"
              KNIFE_SSH_OPTIONS = "-x ${SSH_KEY_USER} -i ${SSH_KEY_PATH}"

              KNIFE_BOOTSTRAP_COMMAND = "knife bootstrap -y --chef-license accept --bootstrap-version '15.14.0' ${KNIFE_COMMON_OPTIONS} ${KNIFE_SSH_OPTIONS} -E ${ENVIRONMENT} -r '%s' -N '%s' %s"
              def bootstrap_command = String.format(KNIFE_BOOTSTRAP_COMMAND, run_list, fqdn, ip)
              sh script: bootstrap_command
            }
          }
        }
      }
    }
  }
  parallel bootstrap_steps
}


if (converge != 'false') {
  node('docker_all') {
    stage('converge') {
      dir('chef') {
        withCredentials(bindings: [
          sshUserPrivateKey(
            credentialsId: CREDENTIAL_JENKINS_SSH,
            keyFileVariable: 'SSH_KEY_PATH',
            usernameVariable: 'SSH_KEY_USER'
          ),
          sshUserPrivateKey(
            credentialsId: CHEF_CREDENTIAL,
            keyFileVariable: 'KNIFE_KEY_PATH',
            usernameVariable: 'KNIFE_NODE_NAME'
          )]
        ) {
          KNIFE_COMMON_OPTIONS = ""
          KNIFE_COMMON_OPTIONS += " --config-option client_key=${KNIFE_KEY_PATH}"
          KNIFE_COMMON_OPTIONS += " --config-option node_name=${KNIFE_NODE_NAME}"
          KNIFE_COMMON_OPTIONS += " --config-option chef_server_url=${CHEF_SERVER_URL}"
          KNIFE_SSH_OPTIONS = "-x ${SSH_KEY_USER} -i ${SSH_KEY_PATH}"

          sh script: "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} 'roles:k8s_base AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} 'chef-client'"
        }
      }
    }
  }
}

if (kubeadm_init != 'false') {
  node('docker_all') {
    stage('kubeadm init') {
      dir('chef') {
        def control_plane_endpoint = HOSTS['apiservers'][0]['fqdn'] + ':6443'
        def kubeadm_init_command = String.format(KUBEADM_INIT_COMMAND, control_plane_endpoint)
        withCredentials(bindings: [
          sshUserPrivateKey(
            credentialsId: CREDENTIAL_JENKINS_SSH,
            keyFileVariable: 'SSH_KEY_PATH',
            usernameVariable: 'SSH_KEY_USER'
          )
        ]) {
          def ca_key_base64 = sh(returnStdout: true, script: "aws s3 cp s3://yapo-k8s-configs/certs/ca/dev.key - |base64 -w0")
          def ca_crt_base64 = sh(returnStdout: true, script: "aws s3 cp s3://yapo-k8s-configs/certs/ca/dev.crt - |base64 -w0")

          println('ca.key')
          println(ca_key_base64)
          println('ca.crt')
          println(ca_crt_base64)

          def MKDIR_KUBERNETS_PKI_DIR = 'mkdir -p /etc/kubernetes/pki'
          sh script: String.format(SSH_COMMAND, SSH_KEY_PATH, SSH_KEY_USER, master['ip'], MKDIR_KUBERNETS_PKI_DIR)

          def CA_KEY_COMMAND = "echo ${ca_key_base64} |base64 -d > /etc/kubernetes/pki/ca.key"
          sh script: String.format(SSH_COMMAND, SSH_KEY_PATH, SSH_KEY_USER, master['ip'], CA_KEY_COMMAND)

          def CA_CRT_COMMAND = "echo ${ca_crt_base64} |base64 -d > /etc/kubernetes/pki/ca.crt"
          sh script: String.format(SSH_COMMAND, SSH_KEY_PATH, SSH_KEY_USER, master['ip'], CA_CRT_COMMAND)

          sh script: String.format(SSH_COMMAND, SSH_KEY_PATH, SSH_KEY_USER, master['ip'], kubeadm_init_command)
        }
      }
    }
  }
}

if (network_init != 'false') {
  node('docker_all') {
    stage('network init') {
      dir('chef') {
        withCredentials(bindings: [
          sshUserPrivateKey(
            credentialsId: CREDENTIAL_JENKINS_SSH,
            keyFileVariable: 'SSH_KEY_PATH',
            usernameVariable: 'SSH_KEY_USER'
          )
        ]) {
          def version_base64 = sh(returnStdout: true, script: String.format(SSH_COMMAND, SSH_KEY_PATH, SSH_KEY_USER, master['ip'], SSH_VERSION_COMMAND))
          def kubeadm_network_command = String.format(KUBEADM_NETWORK_COMMAND, version_base64)
          sh script: String.format(SSH_COMMAND, SSH_KEY_PATH, SSH_KEY_USER, master['ip'], kubeadm_network_command)
        }
      }
    }
  }
}

if (node_join != 'false') {
  node('docker_all') {
    stage('node join') {
      dir('chef') {
        withCredentials(bindings: [
          sshUserPrivateKey(
            credentialsId: CREDENTIAL_JENKINS_SSH,
            keyFileVariable: 'SSH_KEY_PATH',
            usernameVariable: 'SSH_KEY_USER'
          ),
          sshUserPrivateKey(
            credentialsId: CHEF_CREDENTIAL,
            keyFileVariable: 'KNIFE_KEY_PATH',
            usernameVariable: 'KNIFE_NODE_NAME'
          )
        ]) {
          KNIFE_COMMON_OPTIONS = ""
          KNIFE_COMMON_OPTIONS += " --config-option client_key=${KNIFE_KEY_PATH}"
          KNIFE_COMMON_OPTIONS += " --config-option node_name=${KNIFE_NODE_NAME}"
          KNIFE_COMMON_OPTIONS += " --config-option chef_server_url=${CHEF_SERVER_URL}"
          KNIFE_SSH_OPTIONS = "-x ${SSH_KEY_USER} -i ${SSH_KEY_PATH}"

          def node_join_command = sh(returnStdout: true, script: String.format(SSH_COMMAND, SSH_KEY_PATH, SSH_KEY_USER, master['ip'], KUBEADM_JOIN_COMMAND))
          sh script: "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} 'roles:k8s_node AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${node_join_command}'"
        }
      }
    }
  }
}

if (rook_init != 'false') {
  node('docker_all') {
    stage('clone repo') {
      withCredentials(bindings: [
        sshUserPrivateKey(
          credentialsId: CREDENTIAL_JENKINS_SSH,
          keyFileVariable: 'SSH_KEY_PATH',
          usernameVariable: 'SSH_KEY_USER'
        )
      ]) {
        command = "rm -rf /tmp/rook && git clone https://github.com/rook/rook.git /tmp/rook && cd /tmp/rook && git checkout ${ROOK_RELEASE}"
        sh script: String.format(SSH_COMMAND, SSH_KEY_PATH, SSH_KEY_USER, master['ip'], command)

        command = "${KUBECTL_BASE_COMMAND} kubectl create -f /tmp/rook/cluster/examples/kubernetes/ceph/common.yaml || true"
        sh script: String.format(SSH_COMMAND, SSH_KEY_PATH, SSH_KEY_USER, master['ip'], command)

        command = "${KUBECTL_BASE_COMMAND} kubectl create -f /tmp/rook/cluster/examples/kubernetes/ceph/operator.yaml || true"
        sh script: String.format(SSH_COMMAND, SSH_KEY_PATH, SSH_KEY_USER, master['ip'], command)

        command = "sed 's/#deviceFilter:/deviceFilter: vdb/g' -i /tmp/rook/cluster/examples/kubernetes/ceph/cluster.yaml"
        sh script: String.format(SSH_COMMAND, SSH_KEY_PATH, SSH_KEY_USER, master['ip'], command)

        command = "${KUBECTL_BASE_COMMAND} kubectl create -f /tmp/rook/cluster/examples/kubernetes/ceph/cluster.yaml || true"
        sh script: String.format(SSH_COMMAND, SSH_KEY_PATH, SSH_KEY_USER, master['ip'], command)

        command = "${KUBECTL_BASE_COMMAND} kubectl create -f /tmp/rook/cluster/examples/kubernetes/ceph/toolbox.yaml || true"
        sh script: String.format(SSH_COMMAND, SSH_KEY_PATH, SSH_KEY_USER, master['ip'], command)

        command = "${KUBECTL_BASE_COMMAND} kubectl create -f /tmp/rook/cluster/examples/kubernetes/ceph/csi/rbd/storageclass.yaml || true"
        sh script: String.format(SSH_COMMAND, SSH_KEY_PATH, SSH_KEY_USER, master['ip'], command)

      }
    }
  }
}

if (upload_admin_kubeconfig != 'false') {
  node('docker_all') {
    stage('upload_admin_config') {
      withCredentials(bindings: [
        sshUserPrivateKey(
          credentialsId: CREDENTIAL_JENKINS_SSH,
          keyFileVariable: 'SSH_KEY_PATH',
          usernameVariable: 'SSH_KEY_USER'
        )
      ]) {
        command = "cat /etc/kubernetes/admin.conf"
        admin_config = sh(returnStdout: true, script: String.format(SSH_COMMAND, SSH_KEY_PATH, SSH_KEY_USER, master['ip'], command))
        def filepath = "/tmp/admin-${CONTEXT_CLUSTER}-kubeconfig.yaml"
        writeFile file: filepath, text: admin_config
        sh "cat ${filepath} |sed 's/kubernetes-admin@kubernetes/k8sdev/g' |aws s3 cp - s3://yapo-k8s-configs/admin-${CONTEXT_CLUSTER}-kubeconfig.yaml"
      }
    }
  }
}

if (ingress_controller_init == 'true') {
  node('docker_all') {
    stage('descargar kubeconfig chef') {
      sh "helm3 repo add stable https://kubernetes-charts.storage.googleapis.com/"
      sh "aws s3 cp s3://yapo-k8s-configs/admin-${CONTEXT_CLUSTER}-kubeconfig.yaml ~/.kube/config"
      sh """
cat <<-EOF | helm3 install --namespace default nginx-ingress stable/nginx-ingress -f -
controller:
  kind: DaemonSet
  config:
    use-forwarded-headers: "true"
  service:
    type: NodePort
    nodePorts:
      http: "30080"
      https: "30443"
  extraArgs:
    v: 1
  metrics:
    enabled: true
    service:
      annotations:
        prometheus.io/scrape: "true"
        prometheus.io/port: "10254"
EOF
"""
    }
  }
}

if (prometheus_init == 'true') {
  node('docker_all') {
    stage('prometheus init') {
      sh "helm3 repo add stable https://kubernetes-charts.storage.googleapis.com/"
      sh "aws s3 cp s3://yapo-k8s-configs/admin-${CONTEXT_CLUSTER}-kubeconfig.yaml ~/.kube/config"
      install_command = "helm3 install prometheus --version 11.11.0 stable/prometheus" +
        " --namespace default" +
        " --set server.ingress.enabled=true" +
        " --set server.ingress.hosts={prometheus-${CONTEXT_CLUSTER}.dev.yapo.cl}" +
        " --set server.retention=1d" +
        " --set alertmanager.enabled=false" +
        " --set nodeExporter.enabled=true" +
        " --set kubeStateMetrics.enabled=true" +
        " --set pushgateway.enabled=false" +
        " --set server.persistentVolume.size=2Gi" +
        " --set server.persistentVolume.storageClass=rook-ceph-block"
      sh script: install_command
    }
  }
}
if (grafana_init == 'true') {
  node('docker_all') {
    stage('grafana init') {
      sh "helm3 repo add bitnami https://charts.bitnami.com/bitnami"
      sh "aws s3 cp s3://yapo-k8s-configs/admin-${CONTEXT_CLUSTER}-kubeconfig.yaml ~/.kube/config"
      sh """
cat > /tmp/datasources.yaml <<-EOF
apiVersion: 1
datasources:
- name: prometheus
  type: prometheus
  url: http://prometheus-server
  isDefault: true
  access: proxy
EOF

kubectl -n default create secret generic grafana-datasources --from-file=/tmp/datasources.yaml || true

cat <<-EOF | helm3 install --namespace default grafana bitnami/grafana -f -
image:
  tag: 6.4.4
persistence:
  enabled: true
  size: 1Gi
  storageClass: rook-ceph-block
ingress:
  enabled: true
  hosts:
    - name: grafana-${CONTEXT_CLUSTER}.dev.yapo.cl
datasources:
  secretName: grafana-datasources
EOF
"""
    }
  }
}
if (prometheus_central_init != 'false') {
  echo "prometheus_central"
}
if (nodeport_proxy_init != 'false') {
  echo "nodeport_proxy_init"
}

