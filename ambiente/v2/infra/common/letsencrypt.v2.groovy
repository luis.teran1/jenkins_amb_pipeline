node('docker_all') {
  CREDENTIAL_CLOUDFLARE_TOKEN = '4939b51d-8e8c-4cd0-99cd-4eab09632f8d'
  stage('certbot') {
    withCredentials([
      string(credentialsId: CREDENTIAL_CLOUDFLARE_TOKEN, variable: 'CLOUDFLARE_TOKEN')
    ]) {
      CERTBOT_COMMAND = "certbot certonly --agree-tos --register-unsafely-without-email --dns-cloudflare --dns-cloudflare-credentials /tmp/.cf.ini -d %s -d '*.%s'"

      sh 'pwd'
      sh 'tree'
      sh 'whoami'
      sh "echo 'dns_cloudflare_api_token=${CLOUDFLARE_TOKEN}' > /tmp/.cf.ini"
      sh 'chmod 0700 /tmp/.cf.ini'
      certbot_command = String.format(CERTBOT_COMMAND, domain, domain)
      sh certbot_command
      sh 'rm -f /tmp/.cf.ini'
      sh String.format('echo -n "fullchain: " && cat /etc/letsencrypt/live/%s/fullchain.pem', domain)
      sh "cat /etc/letsencrypt/live/${domain}/fullchain.pem | aws s3 cp - s3://yapo-k8s-configs/certs/${domain}/fullchain.pem"

      sh String.format('echo -n "privkey: " && cat /etc/letsencrypt/live/%s/privkey.pem', domain)
      sh "cat /etc/letsencrypt/live/${domain}/privkey.pem | aws s3 cp - s3://yapo-k8s-configs/certs/${domain}/privkey.pem"
    }
  }
}