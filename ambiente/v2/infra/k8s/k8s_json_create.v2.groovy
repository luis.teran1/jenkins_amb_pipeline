CLOUDFORMATION_BRANCH = 'feat/namespace-by-cluster'
AMBIENTE_CHOICES = ['dev01', 'dev02', 'dev03', 'dev04', 'dev05', 'dev06', 'dev07', 'dev08', 'dev09']
CONTEXT_CLUSTER_CHOICES = ['k8sdev', 'k8s01', 'k8s02', 'k8s03', 'k8s04', 'k8s05', 'k8s06', 'k8s07', 'k8s08', 'k8s09']
INPUT_JSON_DESCRIPTION = '''
{
  "namespace": "k8s-dev0x",
  "database": [
    "service_name01-db",
    "service_name02-db"
  ],
  "redis": [
    "service_name01-cache",
    "service_name02-cache"
  ],
  "rabbitmq": [
    "rabbitmq-service_name01",
    "rabbitmq-service_name02"
  ],
  "ms": [
    "service_name01",
    "service_name01"
  ],
  "service": {
    "svc01": {
      "endpoint_port": "5432",
      "addresses": ["172.21.10x.103"],
      "port": "5432"
    },
    "svc02": {
      "endpoint_port": "5858",
      "addresses": ["172.21.10x.102"],
      "port": "5858"
    }
  },
  "secrets": {
    "secret01": {
      "PANDAS_API_KEY": {
        "excludeBase64": false,
        "valor": "xxyyzz123123"
      }
    },
    "secret02": {
      "PANDAS_API_KEY": {
        "excludeBase64": true,
        "valor": "eHh5eXp6MTIzMTIz"
      }
    }
  },
  "resources": {
    "resource01": {
      "PUMP_DB_HOST": {
        "valor": "db_host",
        "excludeBase64": false
      },
      "PUMP_DB_NAME": {
        "valor": "db_name",
        "excludeBase64": false
      }
    },
    "resource02": {
      "PUMP_DB_HOST": {
        "valor": "db_host",
        "excludeBase64": false
      },
      "PUMP_DB_NAME": {
        "valor": "db_name",
        "excludeBase64": false
      }
    }
  }
}  
'''
properties([
  parameters([
    choice(name: 'ambiente', choices: AMBIENTE_CHOICES, description: ''),
    choice(name: 'context_cluster', choices: CONTEXT_CLUSTER_CHOICES, description: ''),
    booleanParam(name: 'create_database', defaultValue: false, description: ''),
    booleanParam(name: 'create_redis', defaultValue: false, description: ''),
    booleanParam(name: 'create_rabbitmq', defaultValue: false, description: ''),
    booleanParam(name: 'create_etcd', defaultValue: false, description: ''),
    booleanParam(name: 'create_secret', defaultValue: false, description: ''),
    booleanParam(name: 'create_resource', defaultValue: false, description: ''),
    booleanParam(name: 'create_service', defaultValue: false, description: ''),
    booleanParam(name: 'create_ms', defaultValue: false, description: ''),
    booleanParam(name: 'create_generic', defaultValue: false, description: ''),
    booleanParam(name: 'create_pvc', defaultValue: false, description: ''),
    text(name: 'inputJson', defaultValue: '', description: INPUT_JSON_DESCRIPTION)
  ])
])

node('docker_all') {
  CONTEXT_CLUSTER = context_cluster
  ENVIRONMENT = ambiente
  CREDENTIAL_ARTIFACTORY_SRV_YAP_DEVOPS = '81390724-b583-4459-9a5e-7893c15ea58b'
  CREDENTIAL_GIT_SRV_YAP_DEVOPS = '9c4e41a9-6dbf-426e-bd2a-f1d02d2fcd69'
  stage('check') {
    git branch: 'feat/namespace-by-cluster',
      credentialsId: CREDENTIAL_GIT_SRV_YAP_DEVOPS,
      url: 'git@github.mpi-internal.com:Yapo/cloudformation.git'

    dir('k8s') {
      sh "mkdir -p ~/.kube"
      sh "aws s3 cp s3://yapo-k8s-configs/admin-${CONTEXT_CLUSTER}-kubeconfig.yaml ~/.kube/config"
      sh label: '', script: "kubectl config current-context && kubectl get nodes"
    }
    dir('values-k8s') {
      git branch: 'master',
        credentialsId: CREDENTIAL_GIT_SRV_YAP_DEVOPS,
        url: 'git@github.mpi-internal.com:Yapo/yapo-k8s-values.git'
    }
    def valuesGeneric = []
    def definicion_namespace = ""
    def namespace = "k8s-${ENVIRONMENT}"
    dir('k8s/resource_k8s/all') {
      if (inputJson.length() > 0) {
        definicion_namespace = readJSON text: inputJson
        patchConfigFile(ENVIRONMENT, definicion_namespace)
      } else {
        def configFile = "${ENVIRONMENT}.json"
        definicion_namespace = readJSON file: configFile
      }

      println("definicion_namespace:\n${definicion_namespace}")

      updateHelmRepo()
      if (create_etcd == 'true') {
        if (definicion_namespace.etcd_enable) {
          createEtcd(namespace)
        }
      }
      if (create_generic == 'true') {
        if (definicion_namespace.generic) {
          for (i = 0; i < definicion_namespace.generic.size(); i++) {
            //println(definicion_namespace.generic[i])
            addGenericChart(namespace, definicion_namespace.generic[i])
          }
        }
      }
      if (create_database == 'true') {
        if (definicion_namespace.database) {
          for (i = 0; i < definicion_namespace.database.size(); i++) {
            //println(definicion_namespace.database[i])
            createDatabase(namespace, definicion_namespace.database[i])
          }
        }
      }
      if (create_redis == 'true') {
        if (definicion_namespace.redis) {
          for (i = 0; i < definicion_namespace.redis.size(); i++) {
            println(definicion_namespace.redis[i])
            createRedis(namespace, definicion_namespace.redis[i])
          }
        }
      }
      if (create_rabbitmq == 'true') {
        if (definicion_namespace.rabbitmq) {
          for (i = 0; i < definicion_namespace.rabbitmq.size(); i++) {
            createRabbitmq(namespace, definicion_namespace.rabbitmq[i])
          }
        }
      }
      if (create_secret == 'true' || create_service == 'true' || create_resource == 'true') {
        if (definicion_namespace.secrets) {
          for (secret in definicion_namespace.secrets) {
            valuesGeneric.add(createSecret(namespace, secret))
          }
        }
        if (definicion_namespace.service) {
          for (service in definicion_namespace.service) {
            valuesGeneric.add(createService(namespace, service))
          }
        }
        if (definicion_namespace.resources) {
          for (resource in definicion_namespace.resources) {
            valuesGeneric.add(createSecret(namespace, resource))
          }
        }
        for (i = 0; i < valuesGeneric.size(); i++) {
          crearYAML(valuesGeneric[i])
          addSecret(valuesGeneric[i].fullnameOverride, namespace)
        }
      }
      if (create_pvc == 'true') {
        if (definicion_namespace.pvc) {
          for (i = 0; i < definicion_namespace.pvc.size(); i++) {
            println(definicion_namespace.pvc[i])
            addPcv(namespace, definicion_namespace.pvc[i].name, definicion_namespace.pvc[i].size, definicion_namespace.pvc[i].storageClass)
          }
        }
      }
    }
    if (create_ms == 'true') {
      if (definicion_namespace.ms) {
        for (i = 0; i < definicion_namespace.ms.size(); i++) {
          def version = getChartVersion(definicion_namespace.ms[i])
          def chartTgz = definicion_namespace.ms[i]
          def downloadName = definicion_namespace.ms[i]

          println(version)
          println(chartTgz)
          downloadChart(chartTgz, version, downloadName)

          createMS(
            namespace,
            definicion_namespace.ms[i],
            "${definicion_namespace.ms[i]}-${version}.tgz",
            "values-k8s/${definicion_namespace.ms[i]}"
          )
        }
      }
    }
    deleteDir()
  }
}


def patchConfigFile(ENVIRONMENT, definicion_namespace) {
  def configFile = "${ENVIRONMENT}.json"
  definicion_original = readJSON file: configFile

  def feature_in_array = ['ms', 'database', 'redis', 'rabbitmq']
  for (feature in feature_in_array) {
    if (definicion_namespace[feature]) {
      def feature_element = null
      for (i = 0; i < definicion_namespace[feature].size(); i++) {
        feature_element = definicion_namespace[feature][i]
        if (!definicion_original[feature].contains(feature_element)) {
          definicion_original[feature].push(feature_element)
        }
      }
    }
  }

  def feature_in_dictionary = ['service', 'secrets', 'resources']
  for (feature in feature_in_dictionary) {
    if (definicion_namespace[feature]) {
      def feature_element_key = null
      def feature_element_value = null
      for (feature_element in definicion_namespace[feature]) {
        feature_element_key = feature_element.key
        feature_element_value = feature_element.value

        if (!definicion_original[feature][feature_element_key]) {
          definicion_original[feature][feature_element_key] = feature_element_value
        }
      }
    }
  }

  writeJSON file: "/tmp/${ENVIRONMENT}.json0", json: definicion_original, pretty: 2
  sh "cat /tmp/${ENVIRONMENT}.json0 | python3 -m json.tool > ${ENVIRONMENT}.json"
  sh "cat ${ENVIRONMENT}.json"

  sshagent([CREDENTIAL_GIT_SRV_YAP_DEVOPS]) {
    try {
      sh 'git config --global user.email "srv.yap.devops@adevinta.com" && git config --global user.name "srv.yap.devops"'
      sh "git commit ${ENVIRONMENT}.json -m 'patch: ${ENVIRONMENT}'"
      sh "git branch --set-upstream-to=origin/${CLOUDFORMATION_BRANCH} ${CLOUDFORMATION_BRANCH}"
      sh 'git fetch && git pull --rebase && git push'
    } catch (err) {

    }
  }
}


def createRabbitmq(namespace, nombreRabbitMQ) {
  try {
    sh "helm3 upgrade --install ${nombreRabbitMQ} bitnami/rabbitmq" +
      " --namespace ${namespace}" +
      " --set fullnameOverride=${nombreRabbitMQ}" +
      " --set auth.username=rabbitmq" +
      " --set auth.password=rabbitmq" +
      " --set persistence.enabled=false"
  } catch (err) {
  }
}


def createRedis(namespace, nombreRedis) {
  sh "helm3 upgrade --install ${nombreRedis} bitnami/redis" +
    " --namespace ${namespace}" +
    " --set fullnameOverride=${nombreRedis}" +
    " --set usePassword=false" +
    " --set cluster.enabled=false" +
    " --set master.persistence.enabled=false"
}


def updateHelmRepo() {
  sh 'helm3 repo add bitnami https://charts.bitnami.com/bitnami'
}

def createEtcd(ambiente) {
  sh "helm3 upgrade --install etcd-server bitnami/etcd" +
    " --set image.debug=true" +
    " --namespace ${ambiente}" +
    " --set fullnameOverride=etcd-server" +
    " --set persistence.enabled=true" +
    " --set auth.rbac.enabled=false" +
    " --set image.tag=3.3.8" +
    " --set statefulset.replicaCount=1" +
    " --set persistence.storageClass=rook-ceph-block" +
    " --set persistence.size=1Gi"
}

def addGenericChart(namespace, config) {
  params = "--set ${config['parameters'].join(' --set ')}"
  sh "helm3 install ${config['release']}  ${config['chart']} ${params} --namespace ${namespace}"
}


def createDatabase(namespace, nombreBD) {
  try {
    sh "helm3 upgrade --install ${nombreBD} bitnami/postgresql" +
      " --set image.debug=true" +
      " --namespace ${namespace}" +
      " --set fullnameOverride=${nombreBD}" +
      " --set global.postgresql.postgresqlUsername=postgres" +
      " --set global.postgresql.postgresqlPassword=postgres" +
      " --set postgresqlPassword=postgres" +
      " --set global.postgresql.postgresqlDatabase=${nombreBD}" +
      " --set persistence.enabled=true" +
      " --set persistence.size=1Gi" +
      " --set persistence.storageClass=rook-ceph-block"

    sh """
cat <<-EOF |helm3 upgrade --install resource-${nombreBD} ../k8s_templetes -f - 
namespace: ${namespace}
fullnameOverride: resource-${nombreBD}
secrets:
  HOST: ${nombreBD}
  PORT: "5432"
  NAME: ${nombreBD}
  USER: postgres
  PASS: postgres
  DB: ${nombreBD}
EOF"""
  } catch (err) {
  }
}


def createSecret(namespace, data) {
  nombre_secreto = data.key
  println(nombre_secreto)
  descripcion_secreto = data.value
  def excludeBase64 = []
  def secrets = [:]
  for (llaves in descripcion_secreto) {
    println("llave " + llaves.key)
    println("valor " + llaves.value.valor)
    secrets.put(llaves.key, llaves.value.valor)
    if (llaves.value.excludeBase64) {
      excludeBase64.add(llaves.key)
    }
  }
  def values = [
    'namespace'       : namespace,
    'fullnameOverride': nombre_secreto,
    'excludeBase64'   : excludeBase64,
    'secrets'         : secrets
  ]
  return values
}

def createService(ambiente, data) {
  nombre_servicio = data.key
  descripcion_servicio = data.value

  def service_map = [
    'port': descripcion_servicio.port,
    'type': 'ClusterIP'
  ]
  def endpoints = [
    'port'     : descripcion_servicio.endpoint_port,
    'addresses': descripcion_servicio.addresses
  ]
  def values = [
    'namespace'       : ambiente,
    'fullnameOverride': nombre_servicio,
    'service'         : service_map,
    'endpoints'       : endpoints
  ]
  return values
}

def crearYAML(data) {
  writeYaml file: 'secret.yaml', data: data, overwrite: true
  sh 'cat secret.yaml'
}

def addSecret(secretName, namespace) {
  try {
    add_secret_commnad = "helm3 upgrade --install ${secretName} ../k8s_templetes -f secret.yaml --namespace ${namespace}"
    println(add_secret_commnad)
    sh add_secret_commnad
  } catch (errr) {

  }
}

def getChartVersion(nombreMS) {
  sh 'pwd'
  def values_base_path = "values-k8s/${nombreMS}"
  def devValuesFilepath = "${values_base_path}/values.dev.yaml"
  def preValuesFilepath = "${values_base_path}/values.pre.yaml"
  def yaml = ""

  if (fileExists(devValuesFilepath)) {
    yaml = readYaml file: devValuesFilepath
  } else {
    yaml = readYaml file: preValuesFilepath
  }
  return yaml.globals.chart_version
}

def downloadChart(chartTgz, version, nombreMS) {
  sh 'rm -rf *.tgz'
  withCredentials([
    usernamePassword(
      credentialsId: CREDENTIAL_ARTIFACTORY_SRV_YAP_DEVOPS,
      usernameVariable: 'ARTIFACTORY_USERNAME',
      passwordVariable: 'ARTIFACTORY_PASSWORD')
  ]) {
    download_command = "curl -u ${ARTIFACTORY_USERNAME}:${ARTIFACTORY_PASSWORD} 'https://artifactory.mpi-internal.com/artifactory/helm-local/yapo/${chartTgz}-${version}.tgz' -o ${nombreMS}-${version}.tgz"
    println(download_command)
    sh script: download_command
  }
}

def createMS(namespace, nombre, chart, values_base_path) {
  try {
    def devValuesFilepath = "${values_base_path}/values.dev.yaml"
    def preValuesFilepath = "${values_base_path}/values.pre.yaml"
    def replacedValuesPath = "/tmp/${nombre}.values.yaml"

    sh "tree ${values_base_path}"

    // check if there is a values.dev.yaml file for the service
    if (fileExists(devValuesFilepath)) {
      sh "cp ${devValuesFilepath} ${replacedValuesPath}"
      sed_command = "sed 's/{dev0x}/${ENVIRONMENT}/g' -i ${replacedValuesPath}"
    } else {
      sh "cp ${preValuesFilepath} ${replacedValuesPath}"
      sed_command = "sed -E '/(tag:|repository:|resource-|${nombre}-|paths:|- path)/!s/pre/${ENVIRONMENT}/g' -i ${replacedValuesPath}"
    }

    echo sed_command
    sh label: '', script: sed_command

    install_command = "helm3 upgrade --install --debug ${nombre} ${chart}" +
      " -f ${replacedValuesPath}" +
      " --namespace ${namespace}" +
      " --set image.debug=true" +
      " --set globals.env=${ENVIRONMENT}"

    println(install_command)
    sh script: install_command
  } catch (err) {
  }
}

def addPcv(namespace, name, size, storageClass) {
  sh """
cat <<-EOF |helm3 upgrade --install ${name} ../k8s_templetes -f - 
namespace: ${namespace}
fullnameOverride: ${name}
storage:
  class: ${storageClass}
  size: ${size}
EOF"""
}