node('docker_all') {
  stage('Git creacion ambiente') {
    deleteDir()
    git branch: 'feat/namespace-by-cluster',
      credentialsId: '71c4a739-452c-4971-a55e-718de21816d1',
      url: 'git@github.mpi-internal.com:Yapo/cloudformation.git'
    dir('k8s') {
      sh "mkdir -p ~/.kube"
      sh "aws s3 cp s3://yapo-k8s-configs/admin-${CONTEXT_CLUSTER}-kubeconfig.yaml ~/.kube/config"
      sh label: '', script: "kubectl config current-context && kubectl get nodes"
    }
    dir('k8s/namespace') {
      sh label: '', script: "sed -i 's/{ambiente}/${ambiente}/g' k8s.v2.tf"
      apiserver_fqdn = "apiserver-${CONTEXT_CLUSTER}.dev.yapo.cl"
      apiserver_ip = InetAddress.getByName(apiserver_fqdn).address.collect { it & 0xFF }.join('.')
      apiserver_url = "https://${apiserver_fqdn}:6443"

      withCredentials([file(credentialsId: 'config_docker', variable: 'config_docker')]) {
        tf_var_list = ''
        tf_var_list += ' -var="docker=' + config_docker + '"'
        tf_var_list += ' -var="apiserver_url=' + apiserver_url + '"'
        tf_var_list += ' -var="k8s_context=k8sdev"'
        tf_var_list += ' -var="ambiente=' + ambiente + '"'

        sh "terraform init ${tf_var_list}"
        sh "terraform destroy -auto-approve ${tf_var_list}"
        if (only_destroy == 'false') {
          retry(count: 5) {
            sh "terraform apply -auto-approve ${tf_var_list}"
          }
          retry(count: 5) {
            sh "./script_crear_service_user.v2.sh ${ambiente}"
            sh "./script_crear_admin_user.v2.sh ${ambiente}"
          }
          retry(count: 5) {
            sh "./_export-kubeconfig.sh chef-${ambiente} -n k8s-${ambiente} > /tmp/chef-${ambiente}-kubeconfig.yaml"
            sh "./_export-kubeconfig.sh admin -n k8s-${ambiente} > /tmp/admin-${ambiente}-kubeconfig.yaml"
            if(upload_kubeconfig == 'true') {
              sh "aws s3 cp /tmp/admin-${ambiente}-kubeconfig.yaml s3://yapo-k8s-configs/admin-${ambiente}-kubeconfig.yaml || echo '[WARGNING] aws s3 cp admin'"
              sh "aws s3 cp /tmp/chef-${ambiente}-kubeconfig.yaml s3://yapo-k8s-configs/chef-${ambiente}-kubeconfig.yaml || echo '[WARGNING] aws s3 cp chef'"
            }
          }
        }
        if (only_destroy == 'false' && upload_chef == 'true') {
          token = sh(returnStdout: true, script: "cat /tmp/chef-${ambiente}-kubeconfig.yaml | grep token:")
          token = token.split()[1].trim()
          build job: 'upload_to_chef',
            parameters: [
              string(name: 'token_k8s', value: token),
              string(name: 'ingress_ip', value: apiserver_ip),
              string(name: 'ambiente_nombre', value: ambiente),
              string(name: 'v2', value: 'true'),
              booleanParam(name: 'update_repo', value: false)
            ],
            wait: true
        }
      }
    }
  }
}
