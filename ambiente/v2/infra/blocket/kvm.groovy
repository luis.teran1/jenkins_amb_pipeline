ENVIRONMENT_NUMBER = environment_number
KVM_NODE = kvm_node
CONTEXT_CLUSTER = context_cluster
BLOCKET_BRANCH = blocket_branch

VIRT_INSTALL_DELTA = 6
ENVIRONMENT_CONTEXT = "${ENVIRONMENT_NUMBER}".padLeft(2, '0')
CONTEXT_DOMAIN = ".dev${ENVIRONMENT_CONTEXT}.yapo.cl"
ENVIRONMENT = "dev${ENVIRONMENT_CONTEXT}"
GATEWAY = "172.21.0.1"
NETMASK = "255.255.0.0"
DNS = "172.21.10.99"

VERSION_SITIO = version_sitio
VERSION_NGA = version_nga

CHEF_SERVER_URL = "https://chef-server.pro.yapo.cl/organizations/yapo"
CONTEXT_CLASS = "172.21.1${ENVIRONMENT_CONTEXT}."

VIRT_DESTROY_ALL = "virsh list |grep ${ENVIRONMENT} |awk '{print \$2}' | xargs -I{} /bin/bash -c 'virsh destroy {} && virsh undefine --domain {} --remove-all-storage || true'"

VIRT_DESTROY_COMMAND = "virsh destroy %s && virsh undefine --domain %s --remove-all-storage"

SSH_COMMAND = 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i %s %s@%s "%s"'

CREDENTIAL_GIT_SRV_YAP_DEVOPS = '9c4e41a9-6dbf-426e-bd2a-f1d02d2fcd69'
CHEF_CREDENTIAL = '9289af24-e2f2-4b4d-a4ae-5ab7dbabeb8a'
CREDENTIAL_JENKINS_SSH = '0465c64d-ce73-4808-be49-8133be8e5304'


def initHosts(HOST_LIST) {
  def OUTPUT = [:]
  for (host in HOST_LIST) {
    hostname = host.key
    attributes = host.value

    def disks = []
    def fqdn = hostname + CONTEXT_DOMAIN
    attributes['hostname'] = hostname
    attributes['fqdn'] = fqdn
    attributes['ip'] = CONTEXT_CLASS + attributes['last_octet']
    def DISK_OPTION = "--disk path=/vms/%s,size=%s,format=raw,bus=virtio"
    disks << String.format(DISK_OPTION, fqdn, attributes['os_size'])
    if (attributes['rook_size']) {
      disks << String.format(DISK_OPTION, String.format("%s-rook", fqdn), attributes['rook_size'])
    }
    attributes['disks'] = disks
    OUTPUT[hostname] = attributes
  }
  return OUTPUT
}


def HOSTS = [
  msite  : [
    os_type   : 'centos6',
    os_size   : 8,
    memory    : 2 * 1024,
    cpus      : 2,
    last_octet: "112",
    run_list  : 'recipe[base::yapo_repo],role[msite]'
  ],
  nginx  : [
    os_type   : 'centos7',
    os_size   : 8,
    memory    : 1 * 1024,
    cpus      : 1,
    last_octet: "111",
    run_list  : 'recipe[base::yapo_repo],role[nginx]'
  ],
  cronjob: [
    os_type   : 'centos6',
    os_size   : 8,
    memory    : 2 * 1024,
    cpus      : 2,
    last_octet: "110",
    run_list  : 'recipe[base::yapo_repo],role[jenkins_cronjob]'
  ],
  nga    : [
    os_type   : 'centos6',
    os_size   : 8,
    memory    : 2 * 1024,
    cpus      : 2,
    last_octet: "109",
    run_list  : 'recipe[base::yapo_repo],role[nga]'
  ],
  www    : [
    os_type   : 'centos6',
    os_size   : 8,
    memory    : 2 * 1024,
    cpus      : 2,
    last_octet: "108",
    run_list  : 'recipe[base::yapo_repo],role[www],role[trans],role[controlpanel]'
  ],
  w2trans: [
    os_type   : 'centos6',
    os_size   : 8,
    memory    : 2 * 1024,
    cpus      : 2,
    last_octet: "107",
    run_list  : 'recipe[base::yapo_repo],role[w2trans],recipe[memcached::default]'
  ],
  redis  : [
    os_type   : 'centos6',
    os_size   : 20,
    memory    : 2 * 1024,
    cpus      : 2,
    last_octet: "106",
    run_list  : 'recipe[base::yapo_repo],role[redisdev],role[rabbitmq]'
  ],
  search : [
    os_type   : 'centos6',
    os_size   : 10,
    memory    : 8 * 1024,
    cpus      : 4,
    last_octet: "105",
    run_list  : 'recipe[base::yapo_repo],role[searchdev],role[filterd],role[dav],role[postman]'
  ],
  varnish: [
    os_type   : 'centos7',
    os_size   : 8,
    memory    : 4 * 1024,
    cpus      : 2,
    last_octet: "104",
    run_list  : 'recipe[base::yapo_repo],role[varnish]'
  ],
  db     : [
    os_type    : 'centos7',
    os_size    : 155,
    memory     : 4 * 1024,
    cpus       : 4,
    last_octet : "103",
    run_list   : 'role[base],recipe[postgres::install_centos7],recipe[postgres::remote_pg_base],role[postgres]',
    retry_count: 2
  ],
  bconf  : [
    os_type   : 'centos6',
    os_size   : 8,
    memory    : 2 * 1024,
    cpus      : 2,
    last_octet: "102",
    run_list  : 'recipe[base::yapo_repo],role[bconf]'
  ],
  repo   : [
    os_type   : 'centos6',
    os_size   : 12,
    memory    : 4 * 1024,
    cpus      : 4,
    last_octet: "101",
    run_list  : 'recipe[base::yapo_repo],role[repo],role[logs]'
  ]
]
HOSTS = initHosts(HOSTS)


SED_LIST = [
  [
    'k': '{log.central}',
    'v': HOSTS['repo']['ip']
  ],
  [
    'k': '{repo:ip}',
    'v': HOSTS['repo']['ip']
  ],
  [
    'k': '{filterd:ip}',
    'v': HOSTS['search']['ip']
  ],
  [
    'k': '{nga:ip}',
    'v': HOSTS['nga']['ip']
  ],
  [
    'k': '{dav:ip}',
    'v': HOSTS['search']['ip']
  ],
  [
    'k': '{w2apache:ip}',
    'v': HOSTS['w2trans']['ip']
  ],
  [
    'k': '{www:ip}',
    'v': HOSTS['www']['ip']
  ],
  [
    'k': '{controlpanel:ip}',
    'v': HOSTS['www']['ip']
  ],
  [
    'k': '{nginx:ip}',
    'v': HOSTS['nginx']['ip']
  ],
  [
    'k': '{trans:ip}',
    'v': HOSTS['w2trans']['ip']
  ],
  [
    'k': '{fail:ip}',
    'v': '1.1.1.1'
  ],
  [
    'k': '{main:ip}',
    'v': HOSTS['db']['ip']
  ],
  [
    'k': '{replica:ip}',
    'v': HOSTS['db']['ip']
  ],
  [
    'k': '{memcached:ip}',
    'v': HOSTS['w2trans']['ip']
  ],
  [
    'k': '{nga-redis:ip}',
    'v': HOSTS['redis']['ip']
  ],
  [
    'k': '{apiserverk8s:ip}',
    'v': "${CONTEXT_CLASS}11"
  ],
  [
    'k': '{search:ip}',
    'v': HOSTS['search']['ip']
  ],
  [
    'k': '{bconf:ip}',
    'v': HOSTS['bconf']['ip']
  ],
  [
    'k': '{CONTEXT_CLASS}',
    'v': "${CONTEXT_CLASS}"
  ],
  [
    'k': '{varnish:ip}',
    'v': HOSTS['varnish']['ip']
  ],
  [
    'k': '{rabbitmq:ip}',
    'v': HOSTS['redis']['ip']
  ],
  [
    'k': '{redis:ip}',
    'v': HOSTS['redis']['ip']
  ],
  [
    'k': '{msite:ip}',
    'v': HOSTS['msite']['ip']
  ],
  [
    'k': '{CONTEXT_CLUSTER}',
    'v': CONTEXT_CLUSTER
  ],
  [
    'k': '{CONTEXT_NUMBER}',
    'v': ENVIRONMENT_CONTEXT
  ],
  [
    'k': '{ENVIRONMENT}',
    'v': ENVIRONMENT
  ],
  [
    'k': '{elasticsearch:uri}',
    'v': 'elastic-elasticsearch-master'
  ],
  [
    'k': '{kafka:uri}',
    'v': '172.21.1.95'
  ]
]

def virtDestroyAll(Boolean dry_run = false) {
  if (dry_run == true) {
    println('would have virtDestroyAll')
    return
  }
  node(KVM_NODE) {
    stage('virt_destroy') {
      sh VIRT_DESTROY_ALL
    }
  }
}

def updateChefReplaceList(SED_LIST, Boolean init_dev_file = false) {
  sh 'rm -rf chef'
  sh 'mkdir chef'
  sh 'pwd'
  dir('chef') {
    sh 'rm -rf *'
    git branch: 'master',
      credentialsId: CREDENTIAL_GIT_SRV_YAP_DEVOPS,
      url: 'git@github.mpi-internal.com:Yapo/chef.git'

    if (init_dev_file == true) {
      sh "cp environments/dev0x.json environments/${ENVIRONMENT}.json"
    }
    fecha = sh(returnStdout: true, script: 'date +"%Y-%m-%d %H:%M"').trim()
    sh "sed 's/serial: YYYY-MM-DD HH:MM/serial: ${fecha}/g' -i environments/${ENVIRONMENT}.json"

    for (SED in SED_LIST) {
      sed_key = SED['k']
      sed_val = SED['v']
      sh "sed 's/${sed_key}/${sed_val}/g' -i environments/${ENVIRONMENT}.json"
    }

    withCredentials(bindings: [
      sshUserPrivateKey(
        credentialsId: CHEF_CREDENTIAL,
        keyFileVariable: 'KNIFE_KEY_PATH',
        usernameVariable: 'KNIFE_NODE_NAME'
      )
    ]) {
      def KNIFE_COMMON_OPTIONS = "--config-option client_key=${KNIFE_KEY_PATH} --config-option node_name=${KNIFE_NODE_NAME} --config-option chef_server_url=${CHEF_SERVER_URL}"
      sh "knife upload ${KNIFE_COMMON_OPTIONS} environments/${ENVIRONMENT}.json --chef-repo-path `pwd`"
    }
    sshagent([CREDENTIAL_GIT_SRV_YAP_DEVOPS]) {
      sh 'git config --global user.email "jenkins.yapo@adevinta.com" && git config --global user.name "Jenkins Yapo"'
      sh "git add environments/${ENVIRONMENT}.json"
      sh "git commit environments/${ENVIRONMENT}.json -m 'add: ${ENVIRONMENT}'"
      sh 'git branch --set-upstream-to=origin/master master'
      sh 'git fetch && git pull --rebase && git push'
    }
  }
}

def chefClientAll(Boolean dry_run = false) {
  if (dry_run == true) {
    println('would have chefClientAll')
    return
  }
  node('docker_all') {
    stage('chef-client all') {
      withCredentials(bindings: [
        sshUserPrivateKey(
          credentialsId: CREDENTIAL_JENKINS_SSH,
          keyFileVariable: 'SSH_KEY_PATH',
          usernameVariable: 'SSH_KEY_USER'
        ),
        sshUserPrivateKey(
          credentialsId: CHEF_CREDENTIAL,
          keyFileVariable: 'KNIFE_KEY_PATH',
          usernameVariable: 'KNIFE_NODE_NAME'
        )]
      ) {
        KNIFE_COMMON_OPTIONS = ""
        KNIFE_COMMON_OPTIONS += " --config-option client_key=${KNIFE_KEY_PATH}"
        KNIFE_COMMON_OPTIONS += " --config-option node_name=${KNIFE_NODE_NAME}"
        KNIFE_COMMON_OPTIONS += " --config-option chef_server_url=${CHEF_SERVER_URL}"
        KNIFE_SSH_OPTIONS = "-x ${SSH_KEY_USER} -i ${SSH_KEY_PATH}"

        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} 'chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} 'chef-client'"
      }
    }
  }
}

def updateCertificateFromAWSToChefEnvironment(Boolean dry_run = false) {
  if (dry_run) {
    println('would have updateCertificateFromAWSToChefEnvironment')
    return
  }
  node('docker_all') {
    stage('updateCertificateFromAWSToChefEnvironment') {
      fullchain_base64 = sh(returnStdout: true, script: "aws s3 cp s3://yapo-k8s-configs/certs/dev${ENVIRONMENT_CONTEXT}.yapo.cl/fullchain.pem - | base64 -w0")
      privkey_base64 = sh(returnStdout: true, script: "aws s3 cp s3://yapo-k8s-configs/certs/dev${ENVIRONMENT_CONTEXT}.yapo.cl/privkey.pem - | base64 -w0")
      println(privkey_base64)
      println(fullchain_base64)

      def SED_LIST = [
        [
          'k': '"inline_crt": .*',
          'v': '"inline_crt": "' + fullchain_base64 + '",'
        ],
        [
          'k': '"inline_key": .*',
          'v': '"inline_key": "' + privkey_base64 + '",'
        ],
      ]
      updateChefReplaceList(SED_LIST)
    }
  }
}

if (update_kubernetes_environment == 'true') {
  updateKubernetesEnvironment(SED_LIST)
  return
}
if (update_certificate_from_aws == 'true') {
  updateCertificateFromAWSToChefEnvironment()
  return
}

def k8sNamespace(SED_LIST, Boolean dry_run = false) {
  if (dry_run == true) {
    println('would have k8sNamespace')
    return
  }
  node('docker_all') {
    stage('k8sNamespace') {
      build job: 'terraform_k8s_namespace.v2',
        parameters: [
          string(name: 'upload_chef', value: 'true'),
          string(name: 'CONTEXT_CLUSTER', value: CONTEXT_CLUSTER),
          string(name: 'ambiente', value: ENVIRONMENT),
          string(name: 'upload_kubeconfig', value: 'true')
        ],
        wait: true
    }
  }
  stage("update kubernetes definition for environment ${ENVIRONMENT}") {
    updateKubernetesEnvironment(SED_LIST)
  }
  stage('deploy k8s base') {
    build job: 'k8s_json_create.v2',
      parameters: [
        string(name: 'ambiente', value: ENVIRONMENT),
        string(name: 'context_cluster', value: CONTEXT_CLUSTER),
        string(name: 'create_database', value: 'true'),
        string(name: 'create_redis', value: 'true'),
        string(name: 'create_rabbitmq', value: 'true'),
        string(name: 'create_etcd', value: 'true'),
        string(name: 'create_secret', value: 'true'),
        string(name: 'create_resource', value: 'true'),
        string(name: 'create_service', value: 'true'),
        string(name: 'create_ms', value: 'true'),
        string(name: 'create_generic', value: 'true'),
        string(name: 'create_pvc', value: 'true')
      ],
      wait: true
  }
}


def initCompile(rama_sitio, rama_nga, repo_ip, dry_run = false) {
  if (dry_run == true) {
    println('would have compiled')
    return
  }
  parallel(
    sitio: {
      retry(count: 5) {
        build job: 'compilar_sitio',
          parameters: [
            string(name: 'site_branch', value: rama_sitio),
            string(name: 'ip_repo', value: repo_ip),
            string(name: 'ambiente_nombre', value: ENVIRONMENT),
            string(name: 'v2', value: "true")
          ],
          wait: true
      }
    },
    nga: {
      retry(count: 5) {
        build job: 'compilar_nga',
          parameters: [
            string(name: 'nga_branch', value: rama_nga),
            string(name: 'ip_repo', value: repo_ip),
            string(name: 'ambiente_nombre', value: ENVIRONMENT),
            string(name: 'v2', value: "true")
          ],
          wait: true
      }
    }
  )
}

def fakeCompile(HOSTS, Boolean dry_run = false) {
  if (dry_run == true) {
    println('would have fakeCompile')
    return
  }
  stage('scp rpms') {
    node('docker_all') {
      withCredentials(bindings: [
        sshUserPrivateKey(
          credentialsId: CREDENTIAL_JENKINS_SSH,
          keyFileVariable: 'SSH_KEY_PATH',
          usernameVariable: 'SSH_KEY_USER'
        )
      ]) {
        def SRC_IP = "172.21.10.101"
        def SRC_PATH = "/usr/share/nginx/html/artifacts/rpms/*"
        def DST_PATH = '/opt/repo/yapo'
        def scp_command = "scp -3 -r " +
          "-o UserKnownHostsFile=/dev/null " +
          "-o StrictHostKeyChecking=no -i ${SSH_KEY_PATH} " +
          "${SSH_KEY_USER}@${SRC_IP}:${SRC_PATH} " +
          "${SSH_KEY_USER}@${HOSTS['repo']['ip']}:${DST_PATH}"
        if (dry_run == 'true') {
          println(scp_command)
          return
        }
        sh scp_command
      }
    }
  }
  stage('build repo') {
    sshCommand(HOSTS['repo']['ip'], '/usr/bin/createrepo --update --workers 12 /opt/repo/yapo', dry_run)
  }
}

def chefBootstrap(HOSTS, dry_run = false) {
  def bootstrap_steps = [:]
  for (int i = 0; i < HOSTS.size(); i++) {
    int index = i
    def fqdn = HOSTS[index]['fqdn']
    def ip = HOSTS[index]['ip']
    def run_list = HOSTS[index]['run_list']
    stage("virt_install:${fqdn}") {
      bootstrap_steps["step:${fqdn}"] = {
        node('docker_all') {
          withCredentials(bindings: [
            sshUserPrivateKey(
              credentialsId: CREDENTIAL_JENKINS_SSH,
              keyFileVariable: 'SSH_KEY_PATH',
              usernameVariable: 'SSH_KEY_USER'
            ),
            sshUserPrivateKey(
              credentialsId: CHEF_CREDENTIAL,
              keyFileVariable: 'KNIFE_KEY_PATH',
              usernameVariable: 'KNIFE_NODE_NAME'
            )]
          ) {

            KNIFE_COMMON_OPTIONS = ""
            KNIFE_COMMON_OPTIONS += " --config-option client_key=${KNIFE_KEY_PATH}"
            KNIFE_COMMON_OPTIONS += " --config-option node_name=${KNIFE_NODE_NAME}"
            KNIFE_COMMON_OPTIONS += " --config-option chef_server_url=${CHEF_SERVER_URL}"
            KNIFE_SSH_OPTIONS = "-x ${SSH_KEY_USER} -i ${SSH_KEY_PATH}"

            KNIFE_BOOTSTRAP_COMMAND = "knife bootstrap -y --chef-license accept --bootstrap-version '15.14.0' ${KNIFE_COMMON_OPTIONS} ${KNIFE_SSH_OPTIONS} -E ${ENVIRONMENT} -r '%s' -N '%s' %s"
            def bootstrap_command = String.format(KNIFE_BOOTSTRAP_COMMAND, run_list, fqdn, ip)
            println(bootstrap_command)
            if (dry_run) {
              return
            }
            retry(count: 5) {
              sh script: bootstrap_command
            }
          }
        }
      }
    }
  }
  parallel bootstrap_steps
}

def virtDestroy(HOSTS, Boolean dry_run = false) {
  node(KVM_NODE) {
    for (int i = 0; i < HOSTS.size(); i++) {
      int index = i
      def fqdn = HOSTS[index]['fqdn']
      def virt_destory_command = String.format(VIRT_DESTROY_COMMAND, fqdn, fqdn)
      sh virt_destory_command
    }
  }
}

def virtInstall(HOSTS, Boolean dry_run = false, clone = false) {
  def KS = [
    "centos6": "http://172.21.10.101:8081/ks/centos6-static.txt",
    "centos7": "http://172.21.10.101:8081/ks/centos7-static.txt"
  ]
  def VIRT_INSTALL_COMMAND = [
    centos7: 'sleep %s && virt-install --name %s --memory %s --vcpus=%s --location=/var/lib/libvirt/images/CentOS-7-x86_64-Minimal-2003.iso %s --network bridge=br0,model=virtio --extra-args "text ip=%s::%s:%s:%s:eth0:none nameserver=%s inst.ks=%s"',
    centos6: 'sleep %s && virt-install --name %s --memory %s --vcpus=%s --location=/var/lib/libvirt/images/CentOS-6.10-x86_64-minimal.iso %s --network bridge=br0,model=virtio --extra-args "text ip=%s::%s ks=%s"'
  ]
  def VIRT_CLONE_COMMAND = 'sleep %s && virt-clone --original %s --name %s %s'

  def virt_install_steps = [:]
  def j = 0
  for (int i = 0; i < HOSTS.size(); i++) {
    int index = i
    def fqdn = HOSTS[index]['fqdn']
    def memory = HOSTS[index]['memory']
    def cpus = HOSTS[index]['cpus']
    def disks = HOSTS[index]['disks']
    def ip = HOSTS[index]['ip']
    def os_type = HOSTS[index]['os_type']
    def ks_url = KS[HOSTS[index]['os_type']]

    def virt_command = ''
    def retry_count = 1
    if (clone == true) {
      def original_image = fqdn.replace(ENVIRONMENT, 'clone')
      // esta imagen tiene que bootear con una IP fija en la misma ip, pero x.x.100.x
      // por ejemplo, si quiero levantar 172.21.109.103, la clonada se levanta en 172.21.100.103
      virt_command = String.format(VIRT_CLONE_COMMAND, VIRT_INSTALL_DELTA * j, original_image, fqdn, "--file /vms/${fqdn}")
    } else {
      if (os_type == 'centos6') {
        virt_command = String.format(VIRT_INSTALL_COMMAND[os_type], VIRT_INSTALL_DELTA * j, fqdn, memory, cpus, disks.join(' '), ip, GATEWAY, ks_url)
      } else {
        virt_command = String.format(VIRT_INSTALL_COMMAND[os_type], VIRT_INSTALL_DELTA * j, fqdn, memory, cpus, disks.join(' '), ip, GATEWAY, NETMASK, fqdn, DNS, ks_url)
      }
    }


    j += 1
    if (dry_run) {
      println(virt_command)
      return
    }
    stage("virt_install:${fqdn}") {
      virt_install_steps["step:${fqdn}"] = {
        node(KVM_NODE) {
          retry(count: retry_count) {
            sh virt_command
          }
        }
      }
    }
  }
  parallel virt_install_steps
}


/*
if (virt_install != 'false') {
  for (HOST_KEY in HOSTS.keySet()) {
    virtInstall(HOSTS[HOST_KEY], true)
  }
}
*/

def sshCommand(host_ip, command, Boolean dry_run = false) {
  node('docker_all') {
    withCredentials(bindings: [
      sshUserPrivateKey(
        credentialsId: CREDENTIAL_JENKINS_SSH,
        keyFileVariable: 'SSH_KEY_PATH',
        usernameVariable: 'SSH_KEY_USER'
      )
    ]) {
      def ssh_command = String.format(SSH_COMMAND, env.SSH_KEY_PATH, env.SSH_KEY_USER, host_ip, command)
      if (dry_run == true) {
        println(ssh_command)
        return
      }
      sh ssh_command
    }
  }
}

def updateKubernetesEnvironment(SED_LIST, Boolean dry_run = false) {
  if (dry_run == true) {
    println('would have updateKubernetesEnvironment')
    return
  }
  node('docker_all') {
    BRANCH = 'feat/namespace-by-cluster'
    stage('download repo') {
      git branch: BRANCH,
        credentialsId: '71c4a739-452c-4971-a55e-718de21816d1',
        url: 'git@github.mpi-internal.com:Yapo/cloudformation.git'
      dir('k8s/resource_k8s/all') {
        sh "cp dev0x.json ${ENVIRONMENT}.json"

        for (SED in SED_LIST) {
          sed_key = SED['k']
          sed_val = SED['v']
          sh "sed 's/${sed_key}/${sed_val}/g' -i ${ENVIRONMENT}.json"
        }
      }

      sshagent(['71c4a739-452c-4971-a55e-718de21816d1']) {
        sh 'git config --global user.email "jenkins.yapo@adevinta.com" && git config --global user.name "Jenkins Yapo"'
        sh "git status"
        sh "git add k8s/resource_k8s/all/${ENVIRONMENT}.json"
        sh "git commit k8s/resource_k8s/all/${ENVIRONMENT}.json -m 'add: ${ENVIRONMENT}' || true"
        sh "git branch --set-upstream-to=origin/${BRANCH} ${BRANCH}"
        sh 'git fetch && git pull --rebase && git push'
      }
    }
  }
}

def updateChefEnvironment(HOSTS, SED_LIST, Boolean dry_run = false) {
  if (dry_run == true) {
    println('would have updateChefEnvironment')
    return
  }
  node('docker_all') {
    stage("chef init ${ENVIRONMENT}") {
      stage('create dir') {
        sh 'rm -rf chef'
        sh 'mkdir chef'
        sh 'pwd'
      }
      stage("update chef environment file: ${ENVIRONMENT}") {
        updateChefReplaceList(SED_LIST, true)
      }
    }
    if (VERSION_SITIO || VERSION_NGA) {
      build job: 'upload_to_chef',
        parameters: [
          string(name: 'nga', value: VERSION_NGA),
          string(name: 'sitio', value: VERSION_SITIO),
          string(name: 'ambiente_nombre', value: ENVIRONMENT),
          string(name: 'update_repo', value: 'false'),
          string(name: 'repo', value: HOSTS['repo']['ip']),
          string(name: 'v2', value: "true")
        ],
        wait: true
    }
  }
}

def deployBranch(branch, Boolean dry_run = false) {
  if (dry_run == true) {
    println('would have deployBranch')
    return
  }
  build job: 'deploy.v2',
    parameters: [
      string(name: 'site_branch', value: branch),
      string(name: 'chef_branch', value: 'master'),
      string(name: 'ejecutar_chef', value: 'true'),
      string(name: 'ambiente', value: ENVIRONMENT),
      string(name: 'v2', value: 'true')
    ], wait: true
}

def pasoFinal(HOSTS, Boolean dry_run = false) {
  if (dry_run == true) {
    println('would have pasoFinal')
    return
  }

  node('docker_all') {
    stage("habilitar search , msite, cambio de nombre y creacion de MS") {
      parallel(
        crear_indices_search: {
          build job: 'crear_indice.v2',
            parameters: [
              string(name: 'ambiente', value: ENVIRONMENT),
              string(name: 'tipo_search', value: 'asearch'),
              string(name: 'search', value: HOSTS['search']['ip'])
            ], wait: true
          build job: 'crear_indice.v2',
            parameters: [
              string(name: 'ambiente', value: ENVIRONMENT),
              string(name: 'tipo_search', value: 'csearch'),
              string(name: 'search', value: HOSTS['search']['ip'])
            ], wait: true
          build job: 'crear_indice.v2',
            parameters: [
              string(name: 'ambiente', value: ENVIRONMENT),
              string(name: 'tipo_search', value: 'josesearch'),
              string(name: 'search', value: HOSTS['search']['ip'])
            ], wait: true
          build job: 'crear_indice.v2',
            parameters: [
              string(name: 'ambiente', value: ENVIRONMENT),
              string(name: 'tipo_search', value: 'msearch'),
              string(name: 'search', value: HOSTS['search']['ip'])
            ], wait: true
        },
        Add_api_key_para_msite: {
          build job: 'enable_msite_pipeline.v2',
            parameters: [
              string(name: 'ambiente', value: ENVIRONMENT)
            ],
            wait: true
        }
      )
    }
  }
}
/*
if (bootstrap != 'false') {
  for (HOST_KEY in HOSTS.keySet()) {
    chefBootstrap(HOSTS[HOST_KEY], true)
  }
}
*/

/*
MAIN_STEPS = [
  [
    function  : 'virtDestroy',
    parameters: [HOSTS['bconf']]
  ],
  [
    function  : 'virtInstall',
    parameters: [
      HOSTS['bconf']
    ],
    dry_run   : false
  ],
  [
    function  : 'chefBootstrap',
    parameters: [HOSTS['bconf']],
    dry_run   : false
  ]
]
*/

MAIN_STEPS = [
  [
    function: 'virtDestroyAll'
  ],
  [
    function: 'updateChefEnvironment',
  ],
  [
    function: 'updateCertificateFromAWSToChefEnvironment',
  ],
  [
    function: 'k8sNamespace'
  ],
  [
    function  : 'virtInstall',
    parameters: [
      HOSTS['repo']
    ]
  ],
  [
    function  : 'chefBootstrap',
    parameters: [
      HOSTS['repo']
    ]
  ],
  [
    function  : 'initCompile',
    parameters: [
      BLOCKET_BRANCH,
      'feat/ad_contact_endpoint',
      HOSTS['repo']['ip']]
  ],
  [
    function  : 'virtInstall',
    //TODO[no bootstrapear la maquina de base de datos para clonar el disco]: function  : 'virtInstallClone',
    parameters: [
      HOSTS['db']
    ],

  ],
  [
    function  : 'chefBootstrap',
    parameters: [HOSTS['db']]
  ],
  [
    function  : 'sshCommand',
    parameters: [
      HOSTS['db']['ip'],
      'yum remove blocket* -y && chef-client'
    ]
  ],
  [
    function  : 'virtInstall',
    parameters: [
      HOSTS['bconf'],
    ]
  ],
  [
    function  : 'chefBootstrap',
    parameters: [
      HOSTS['bconf']
    ]
  ],
  [
    function  : 'virtInstall',
    parameters: [
      HOSTS['redis'],
      HOSTS['search']
    ]
  ],
  [
    function  : 'chefBootstrap',
    parameters: [
      HOSTS['redis'],
      HOSTS['search']
    ]
  ],
  [
    function  : 'virtInstall',
    parameters: [
      HOSTS['varnish'],
      HOSTS['nga'],
      HOSTS['nginx'],
      HOSTS['cronjob'],
      HOSTS['msite'],
    ]
  ],
  [
    function  : 'chefBootstrap',
    parameters: [
      HOSTS['varnish'],
      HOSTS['nga'],
      HOSTS['nginx'],
      HOSTS['cronjob'],
      HOSTS['msite']
    ],
  ],
  [
    function  : 'virtInstall',
    parameters: [
      HOSTS['w2trans'],
      HOSTS['www']
    ],
  ],
  [
    function  : 'chefBootstrap',
    parameters: [
      HOSTS['w2trans'],
      HOSTS['www']
    ],
  ],
  [
    function: 'chefClientAll',
  ],
  [
    function  : 'deployBranch',
    parameters: [BLOCKET_BRANCH]
  ],
  [
    function: 'pasoFinal'
  ]
]

def CURRENT_DRY_RUN = true
def f
def p
for (main_step in MAIN_STEPS) {
  f = main_step['function']
  p = main_step['parameters']
  if (run_from == f) {
    CURRENT_DRY_RUN = false
  }
  DRY_RUN = CURRENT_DRY_RUN
  if (main_step.containsKey('dry_run')) {
    DRY_RUN = main_step['dry_run']
  }

  if (f == 'deployBranch') {
    deployBranch(p[0], DRY_RUN)
  }
  if (f == 'chefClientAll') {
    chefClientAll(DRY_RUN)
  }
  if (f == 'k8sNamespace') {
    k8sNamespace(SED_LIST, DRY_RUN)
  }
  if (f == 'virtInstall') {
    virtInstall(p, DRY_RUN)
  }
  if (f == 'initCompile') {
    initCompile(p[0], p[1], p[2], DRY_RUN)
  }
  if (f == 'chefBootstrap') {
    chefBootstrap(p, DRY_RUN)
  }
  if (f == 'updateChefEnvironment') {
    updateChefEnvironment(HOSTS, SED_LIST, DRY_RUN)
  }
  if (f == 'sshCommand') {
    sshCommand(p[0], p[1], DRY_RUN)
  }
  if (f == 'fakeCompile') {
    fakeCompile(HOSTS, DRY_RUN)
  }
  if (f == 'pasoFinal') {
    pasoFinal(HOSTS, DRY_RUN)
  }
  if (f == 'virtDestroy') {
    virtDestroy(p, DRY_RUN)
  }
  if (f == 'virtDestroyAll') {
    virtDestroyAll(DRY_RUN)
  }
  if (f == 'updateCertificateFromAWSToChefEnvironment') {
    updateCertificateFromAWSToChefEnvironment(DRY_RUN)
  }
  if (f == 'virtInstallClone') {
    virtInstall(p, DRY_RUN, true)
  }
}


/*
if (false) {
  node('docker_all') {
    withCredentials(bindings: [
      sshUserPrivateKey(
        credentialsId: CREDENTIAL_JENKINS_SSH,
        keyFileVariable: 'SSH_KEY_PATH',
        usernameVariable: 'SSH_KEY_USER'
      ),
      sshUserPrivateKey(
        credentialsId: CHEF_CREDENTIAL,
        keyFileVariable: 'KNIFE_KEY_PATH',
        usernameVariable: 'KNIFE_NODE_NAME'
      )
    ]) {

      def version_base64 = sh(returnStdout: true, script: String.format(SSH_COMMAND, env.SSH_KEY_PATH, env.SSH_KEY_USER, master['ip'], SSH_VERSION_COMMAND))
      println(version_base64)
      def KNIFE_COMMON_OPTIONS = "--config-option client_key=${KNIFE_KEY_PATH} --config-option node_name=${KNIFE_NODE_NAME} --config-option chef_server_url=${CHEF_SERVER_URL}"
      def KNIFE_SSH_OPTIONS = "-x ${SSH_KEY_USER} -i ${SSH_KEY_PATH}"
      def node_list = sh(returnStdout: true, script: "knife node list ${KNIFE_COMMON_OPTIONS}")
      println(node_list)

      def knife_ssh_output = sh(returnStdout: true, script: "knife ssh ${KNIFE_COMMON_OPTIONS} 'name:master01-k8s01.dev.yapo.cl' -a ipaddress ${KNIFE_SSH_OPTIONS} 'hostname'")
      println(knife_ssh_output)
    }
  }
}
*/
