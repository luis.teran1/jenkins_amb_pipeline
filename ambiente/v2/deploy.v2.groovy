/* job deploy en jenkins-amb */
def TGZFILE = ""
def VERSIONCOMMIT = "22.18.00-dbc1110"
def REPODIR = "/opt/repo/yapo"
def DB_HOST_PORT = ""

ENVIRONMENT = ambiente

def myjobrepo = build job: 'get_ip_nodo',
  parameters: [
    string(name: 'ambiente', value: ambiente),
    string(name: 'role', value: 'repo')
  ],
  propagate: true,
  wait: true

def variablesrepo = myjobrepo.getBuildVariables()
def repo_server = variablesrepo["ip"]
echo repo_server

def myjobbd = build job: 'get_ip_nodo',
  parameters: [
    string(name: 'ambiente', value: ambiente),
    string(name: 'role', value: 'postgres')
  ],
  propagate: true,
  wait: true

def variablesbd = myjobbd.getBuildVariables()
def bd_server = variablesbd["ip"]
echo bd_server

node('docker_all') {
  withCredentials(bindings: [
    sshUserPrivateKey(
      credentialsId: '0465c64d-ce73-4808-be49-8133be8e5304',
      keyFileVariable: 'SSH_KEY_PATH',
      usernameVariable: 'SSH_KEY_USER'
    ),
    sshUserPrivateKey(
      credentialsId: '9289af24-e2f2-4b4d-a4ae-5ab7dbabeb8a',
      keyFileVariable: 'KNIFE_KEY_PATH',
      usernameVariable: 'KNIFE_NODE_NAME'
    )]
  ) {
    stage('download Sitio git') {
      deleteDir()
      build job: 'compilar_sitio',
        parameters: [
          string(name: 'site_branch', value: site_branch),
          string(name: 'ip_repo', value: repo_server),
          string(name: 'ambiente_nombre', value: ambiente),
          string(name: 'v2', value: v2)
        ], wait: true
    }
    if (ejecutar_chef) {
      stage('Step Stop Trans') {
        sh 'whoami'

        CHEF_SERVER_URL = "https://chef-server.pro.yapo.cl/organizations/yapo"

        KNIFE_COMMON_OPTIONS = ""
        KNIFE_COMMON_OPTIONS += " --config-option client_key=${KNIFE_KEY_PATH}"
        KNIFE_COMMON_OPTIONS += " --config-option node_name=${KNIFE_NODE_NAME}"
        KNIFE_COMMON_OPTIONS += " --config-option chef_server_url=${CHEF_SERVER_URL}"
        KNIFE_SSH_OPTIONS = "-x ${SSH_KEY_USER} -i ${SSH_KEY_PATH}"

        query = "(roles:w2trans or roles:controlpanel)"
        command = "/etc/init.d/trans stop"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
      }
      stage('Step BD') {
        build job: 'Yapo.cl_run_migrations',
          parameters: [
            string(name: 'DB_HOST_PORT', value: bd_server + ':5432'),
            string(name: 'branch', value: site_branch),
            string(name: 'ambiente', value: ambiente)
          ],
          wait: true

        CHEF_SERVER_URL = "https://chef-server.pro.yapo.cl/organizations/yapo"

        KNIFE_COMMON_OPTIONS = ""
        KNIFE_COMMON_OPTIONS += " --config-option client_key=${KNIFE_KEY_PATH}"
        KNIFE_COMMON_OPTIONS += " --config-option node_name=${KNIFE_NODE_NAME}"
        KNIFE_COMMON_OPTIONS += " --config-option chef_server_url=${CHEF_SERVER_URL}"
        KNIFE_SSH_OPTIONS = "-x ${SSH_KEY_USER} -i ${SSH_KEY_PATH}"

        query = "(role:postgres)"
        command = "chef-client"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
      }
      stage('Step BCONF') {

        CHEF_SERVER_URL = "https://chef-server.pro.yapo.cl/organizations/yapo"

        KNIFE_COMMON_OPTIONS = ""
        KNIFE_COMMON_OPTIONS += " --config-option client_key=${KNIFE_KEY_PATH}"
        KNIFE_COMMON_OPTIONS += " --config-option node_name=${KNIFE_NODE_NAME}"
        KNIFE_COMMON_OPTIONS += " --config-option chef_server_url=${CHEF_SERVER_URL}"
        KNIFE_SSH_OPTIONS = "-x ${SSH_KEY_USER} -i ${SSH_KEY_PATH}"

        query = "(roles:bconf)"

        command = "yum remove blocket* -y"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        command = "chef-client"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
      }
      stage('Cambiar nombre ambiente en conf') {
        build job: 'cambiar_nombre_dominio.v2',
          parameters: [
            string(name: 'ambiente', value: ENVIRONMENT)
          ],
          wait: true
      }
      stage('Step Todo Blocket') {

        CHEF_SERVER_URL = "https://chef-server.pro.yapo.cl/organizations/yapo"

        KNIFE_COMMON_OPTIONS = ""
        KNIFE_COMMON_OPTIONS += " --config-option client_key=${KNIFE_KEY_PATH}"
        KNIFE_COMMON_OPTIONS += " --config-option node_name=${KNIFE_NODE_NAME}"
        KNIFE_COMMON_OPTIONS += " --config-option chef_server_url=${CHEF_SERVER_URL}"
        KNIFE_SSH_OPTIONS = "-x ${SSH_KEY_USER} -i ${SSH_KEY_PATH}"

        parallel(
          trans: {
            query = "(roles:w2trans)"
            command = 'chef-client'
            sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
          },
          controlpanel: {
            query = "(roles:controlpanel)"
            command = 'chef-client'
            sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
          },
          www: {
            query = "(roles:www)"
            command = 'chef-client'
            sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
          },
          msite: {
            query = "(roles:msite)"
            command = 'chef-client'
            sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
          },
          rabbit: {
            query = "(roles:rabbitmq)"
            command = 'chef-client'
            sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
          },
          search: {
            query = "(role:searchdev)"
            command = 'chef-client'
            sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
          },
          filterd: {
            query = "(role:filterd)"
            command = 'chef-client'
            sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
          },
          redis_nga: {
            query = "(roles:nga-redis)"
            command = 'chef-client'
            sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
          },
          redis: {
            query = "(roles:redis_cardata)"
            command = 'chef-client'
            sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
          }
        )
      }
    }
    stage('Destruir todo') {
      deleteDir()
    }
  }
}