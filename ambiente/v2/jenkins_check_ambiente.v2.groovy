nodo_ejecucion = 'principal'
comando_extra = ' -x root '
chef_comand = 'chef-client '

def myjobSearch = build job: 'get_ip_nodo', parameters: [
  string(name: 'ambiente', value: ambiente_nombre),
  string(name: 'role', value: 'searchdev')
], propagate: true, wait: true
def variablesSearch = myjobSearch.getBuildVariables()
search_ip = variablesSearch["ip"]


node('docker_all') {
  ENVIRONMENT = ambiente_nombre
  withCredentials(bindings: [
    sshUserPrivateKey(
      credentialsId: '0465c64d-ce73-4808-be49-8133be8e5304',
      keyFileVariable: 'SSH_KEY_PATH',
      usernameVariable: 'SSH_KEY_USER'
    ),
    sshUserPrivateKey(
      credentialsId: '9289af24-e2f2-4b4d-a4ae-5ab7dbabeb8a',
      keyFileVariable: 'KNIFE_KEY_PATH',
      usernameVariable: 'KNIFE_NODE_NAME'
    )]
  ) {
    CHEF_SERVER_URL = "https://chef-server.pro.yapo.cl/organizations/yapo"

    KNIFE_COMMON_OPTIONS = ""
    KNIFE_COMMON_OPTIONS += " --config-option client_key=${KNIFE_KEY_PATH}"
    KNIFE_COMMON_OPTIONS += " --config-option node_name=${KNIFE_NODE_NAME}"
    KNIFE_COMMON_OPTIONS += " --config-option chef_server_url=${CHEF_SERVER_URL}"
    KNIFE_SSH_OPTIONS = "-x ${SSH_KEY_USER} -i ${SSH_KEY_PATH}"

    stage('chef-client: bconf') {
      query = "(roles:bconf or roles:bconf_full NOT roles:prometheus)"
      command = "chef-client"
      sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
    }
    stage('chef-client: trans') {
      query = "(roles:trans)"
      command = "chef-client"
      sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
    }
    stage('chef-client: db') {
      query = "(roles:postgres)"
      command = 'chef-client'
      sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
    }
    stage('chef-client: filterd') {
      query = "(roles:filterd)"
      command = 'chef-client'
      sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
    }
    stage('chef-client: search') {
      query = "(roles:searchdev)"
      command = 'chef-client'
      sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
    }
    stage('chef-client: redis-cardata') {
      query = "(roles:redis_cardata)"
      command = 'chef-client'
      sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
    }
    stage('chef-client: nga-redis') {
      query = "(roles:nga-redis)"
      command = 'chef-client'
      sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
    }
    stage('chef-client: rabbitmq') {
      query = "(roles:rabbitmq)"
      command = 'chef-client'
      sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
    }
    stage('chef-client: cronjobs') {
      query = "(roles:jenkins_cronjob or roles:cronjobs)"
      command = 'chef-client'
      sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
    }
    stage('chef-client: www') {
      query = "(roles:www NOT roles:prometheus)"
      command = 'chef-client'
      sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
    }
    stage('chef-client: www2') {
      query = "(roles:www2 or roles:www2_full NOT roles:controlpanel NOT roles:prometheus)"
      command = 'chef-client'
      sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
    }
    stage('chef-client: msite') {
      query = "(roles:msite or roles:msite_full NOT roles:prometheus)"
      command = 'chef-client'
      sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
    }
    stage('chef-client: nga') {
      query = "(roles:nga or roles:nga_full NOT roles:prometheus)"
      command = 'chef-client'
      sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
    }
    stage('chef-client: controlpanel') {
      query = "(roles:controlpanel or roles:controlpanel_full NOT roles:prometheus)"
      command = 'chef-client'
      sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
    }
    stage('chef-client: nginx') {
      query = "(roles:nginx)"
      command = "service nginx restart"
      sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

      if (run_chef_nginx == 'true') {
        command = 'chef-client'
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
      }
    }
    stage('chef-client: varnish') {
      query = "(roles:varnish)"
      command = 'chef-client'
      sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
    }
    stage('chef-client: dav') {
      query = "(roles:dav)"
      command = 'chef-client'
      sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
    }
    stage('Reconstruir Searchs') {
      build job: 'crear_indice.v2',
        parameters: [
          string(name: 'ambiente', value: ENVIRONMENT),
          string(name: 'tipo_search', value: 'asearch'),
          string(name: 'search', value: search_ip)
        ], wait: true
      build job: 'crear_indice.v2',
        parameters: [
          string(name: 'ambiente', value: ENVIRONMENT),
          string(name: 'tipo_search', value: 'csearch'),
          string(name: 'search', value: search_ip)
        ], wait: true
      build job: 'crear_indice.v2',
        parameters: [
          string(name: 'ambiente', value: ENVIRONMENT),
          string(name: 'tipo_search', value: 'josesearch'),
          string(name: 'search', value: search_ip)
        ], wait: true
      build job: 'crear_indice.v2',
        parameters: [
          string(name: 'ambiente', value: ENVIRONMENT),
          string(name: 'tipo_search', value: 'msearch'),
          string(name: 'search', value: search_ip)
        ], wait: true
    }
  }
}
