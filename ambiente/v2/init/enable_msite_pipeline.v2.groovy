node('docker_all') {
  ENVIRONMENT = ambiente
  retry(count: 5) {
    withCredentials(bindings: [
      sshUserPrivateKey(
        credentialsId: '0465c64d-ce73-4808-be49-8133be8e5304',
        keyFileVariable: 'SSH_KEY_PATH',
        usernameVariable: 'SSH_KEY_USER'
      ),
      sshUserPrivateKey(
        credentialsId: '9289af24-e2f2-4b4d-a4ae-5ab7dbabeb8a',
        keyFileVariable: 'KNIFE_KEY_PATH',
        usernameVariable: 'KNIFE_NODE_NAME'
      )]
    ) {
      CHEF_SERVER_URL = "https://chef-server.pro.yapo.cl/organizations/yapo"

      KNIFE_COMMON_OPTIONS = ""
      KNIFE_COMMON_OPTIONS += " --config-option client_key=${KNIFE_KEY_PATH}"
      KNIFE_COMMON_OPTIONS += " --config-option node_name=${KNIFE_NODE_NAME}"
      KNIFE_COMMON_OPTIONS += " --config-option chef_server_url=${CHEF_SERVER_URL}"
      KNIFE_SSH_OPTIONS = "-x ${SSH_KEY_USER} -i ${SSH_KEY_PATH}"

      def query = "(roles:redisdev)"
      def command = ""
      stage('Add api key para msite') {
        command = 'redis-cli -p 6396  HMSET mcp1xapp_id_blocket_mobile api_key 746e1a72b4172e50d9c6b4f981312a1803283a74 expire_time 300 region_limit 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 ad_type_limit b,h,k,s,u request_limit 100000 appl_limit import,importdeletead,linkshelf,list,get_save_searches,get_saved_ads,view_phone,view,areas,categories,category_settings,category_params,sendmail,region_getcommunes,settings,related_ads,newad,sendtip,deletead,findstores,account_login,account_create,account_modify,account_change_password,account_lost_password,account_get_unpublished_ads,account_get_published_ads,account_adwatches,account_get_adwatch_ads,account_info,account_adwatch_create,account_adwatch_delete,account_save_ad,account_delete_save_ad,account_get_saved_ads,account_adwatch_email_start,account_adwatch_email_stop,list_latest,payment_status,payment_choice,account_create_jobb,getregion,getinsertingfee,account_jobb_list,sendpass,cars_data,cars_getmodels,cars_getversions,cars_getattributes,api_conf,delete_reasons,faq_list,support,rules,forgot_pass,my_ads,ad_password,ad_refused cat_limit 1000,2000,3000,4000,5000,6000,7000,8000,9000,1220,1240,1260,2020,2040,2060,2080,2100,2120,3020,3040,3060,3080,4020,4040,4060,4080,5020,5040,5060,5160,6020,6060,6080,6100,6120,6140,6160,6180,7020,7040,7060,7080,8020,9020,9040,9060,1,2,3,4,5,6,7,8,9'
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
      }
      stage('Add api key para partners CM') {

        command = 'redis-cli -p 6396 HMSET mcp1xapp_id_pybpropiedades api_key 667516bdd07fb9865065f8468979c6d8cf079184db38caf5573137bfc27834b0 expire_time 300 region_limit 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 ad_type_limit h,k,s,u request_limit 100000 appl_limit import,importdeletead,newad cat_limit 1000,1220,1240,1260 logging 1'
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"


        command = 'redis-cli -p 6396 HMSET mcp1xapp_id_prourbe api_key 77c52146d2db9be6a4641296daa546d0a7a06faa23d3bc075d13c7659bb6afb0 expire_time 300 region_limit 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 ad_type_limit h,k,s,u request_limit 100000 appl_limit import,importdeletead,newad cat_limit 1000,1220,1240,1260 logging 1'
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        command = 'redis-cli -p 6396 HMSET mcp1xapp_id_propertyhunter api_key 551bdc3dd3cdbce8fe4b26827af13889f31f34b6ffd577a1dbbec930ea8f477f expire_time 300 region_limit 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 ad_type_limit h,k,s,u request_limit 100000 appl_limit import,importdeletead,newad cat_limit 1000,1220,1240,1260 logging 1'
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        command = 'redis-cli -p 6396 HMSET mcp1xapp_id_propartnerspropiedades api_key 5bde4ccb78cfbf52427d6b47853a874eccac63e6caaf90fbf4d8deeef141acb3 expire_time 300 region_limit 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 ad_type_limit h,k,s,u request_limit 100000 appl_limit import,importdeletead,newad cat_limit 1000,1220,1240,1260 logging 1'
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        command = 'redis-cli -p 6396 HMSET mcp1xapp_id_procasa9 api_key e8308db67ae9e8d5ea1f4c2a3016368c expire_time 300 region_limit 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 ad_type_limit h,k,s,u request_limit 100000 appl_limit import,importdeletead,newad cat_limit 1000,1220,1240,1260 logging 1'
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        command = 'redis-cli -p 6396 HMSET mcp1xapp_id_procasa8 api_key 75f7977142b2718f5822f03676bc95e1 expire_time 300 region_limit 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 ad_type_limit h,k,s,u request_limit 100000 appl_limit import,importdeletead,newad cat_limit 1000,1220,1240,1260 logging 1'
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        command = 'redis-cli -p 6396 HMSET mcp1xapp_id_procasa30 api_key 8508f315f0ab230602a09978041c687b expire_time 300 region_limit 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 ad_type_limit h,k,s,u request_limit 100000 appl_limit import,importdeletead,newad cat_limit 1000,1220,1240,1260 logging 1'
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        command = 'redis-cli -p 6396 HMSET mcp1xapp_id_century21valle api_key 7ad23963576f9d55fc41469c3e87409d34f0ca0f9e52d1f4372b0b882f166b9a expire_time 300 region_limit 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 ad_type_limit h,k,s,u request_limit 100000 appl_limit import,importdeletead,newad cat_limit 1000,1220,1240,1260 logging 1'
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        command = 'redis-cli -p 6396 HMSET mcp1xapp_id_century21soca api_key a793e66aef8d4d3cf7ebec68cea532def6e2f0d306512dd31f77c8e53a6d9b63 expire_time 300 region_limit 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 ad_type_limit h,k,s,u request_limit 100000 appl_limit import,importdeletead,newad cat_limit 1000,1220,1240,1260 logging 1'
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
      }
    }
  }
}