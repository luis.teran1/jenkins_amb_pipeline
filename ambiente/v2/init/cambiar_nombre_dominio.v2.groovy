node('docker_all') {
  ENVIRONMENT = ambiente
  CHEF_CREDENTIAL = '9289af24-e2f2-4b4d-a4ae-5ab7dbabeb8a'
  CREDENTIAL_JENKINS_SSH='0465c64d-ce73-4808-be49-8133be8e5304'

  retry(count: 5) {
    withCredentials(bindings: [
      sshUserPrivateKey(
        credentialsId: CREDENTIAL_JENKINS_SSH,
        keyFileVariable: 'SSH_KEY_PATH',
        usernameVariable: 'SSH_KEY_USER'
      ),
      sshUserPrivateKey(
        credentialsId: CHEF_CREDENTIAL,
        keyFileVariable: 'KNIFE_KEY_PATH',
        usernameVariable: 'KNIFE_NODE_NAME'
      )]
    ) {
      CHEF_SERVER_URL = "https://chef-server.pro.yapo.cl/organizations/yapo"

      KNIFE_COMMON_OPTIONS = ""
      KNIFE_COMMON_OPTIONS += " --config-option client_key=${KNIFE_KEY_PATH}"
      KNIFE_COMMON_OPTIONS += " --config-option node_name=${KNIFE_NODE_NAME}"
      KNIFE_COMMON_OPTIONS += " --config-option chef_server_url=${CHEF_SERVER_URL}"
      KNIFE_SSH_OPTIONS = "-x ${SSH_KEY_USER} -i ${SSH_KEY_PATH}"

      def query = "(roles:bconf OR roles:bconf_full)"
      def command = ""
      stage('remplazar bconf') {
        command = "sed -i 's/yapo\\\\.cl/${ENVIRONMENT}.yapo.cl/g' /opt/blocket/conf/*"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        command = "sed -i 's/static.${ENVIRONMENT}.yapo.cl/static.yapo.cl/g' /opt/blocket/conf/*"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        command = "sed -i 's/m_${ENVIRONMENT}.yapo.cl/m_${ENVIRONMENT}_yapo_cl/g' /opt/blocket/conf/*"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        command = "sed -i 's/widgets.${ENVIRONMENT}.yapo.cl/widgets.yapo.cl/g' /opt/blocket/conf/*"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        command = "sed -i '/*.trans.newad.import.import_review_cat.1000/d' /opt/blocket/conf/bconf.txt.site"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        command = "sed -i '/*.trans.newad.import.import_review_cat.1220/d' /opt/blocket/conf/bconf.txt.site"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        command = "sed -i '/*.trans.newad.import.import_review_cat.1240/d' /opt/blocket/conf/bconf.txt.site"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        command = "sed -i '/*.trans.newad.import.import_review_cat.1260/d' /opt/blocket/conf/bconf.txt.site"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        command = "chef-client"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
      }

      stage('reiniciar servicios') {
        query = "(roles:trans OR roles:w2trans OR roles:controlpanel)"
        command = "(while  ps aux | grep '/opt/blocket/bin/trans' | grep -v 'grep'; do /etc/init.d/trans stop && /bin/sleep 20 ; done;)"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        query = "(roles:trans OR roles:w2trans OR roles:controlpanel)"
        command = "/etc/init.d/trans start"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        query = "(roles:www)"
        command = "/etc/init.d/bapache restart"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        query = "(roles:www2 OR roles:www2_full OR roles:w2trans OR roles:controlpanel)"
        command = "/etc/init.d/bapache restart"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        query = "(roles:msite OR roles:msite_full)"
        command = "/etc/init.d/bapache restart"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"

        query = "(roles:nga OR roles:nga_full)"
        command = "/etc/init.d/api restart"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
      }
    }
  }
}
