def TGZFILE = ""
def VERSIONCOMMIT = ""
def VERSIONCOMMITNGA = ""
def REPODIR = "/opt/repo/yapo"
CREDENTIAL_GIT_SRV_YAP_DEVOPS='9c4e41a9-6dbf-426e-bd2a-f1d02d2fcd69'
CREDENTIAL_JENKINS_SSH='0465c64d-ce73-4808-be49-8133be8e5304'

node('docker') {
  stage('download NGA git') {
    git branch: '${nga_branch}',
      credentialsId: CREDENTIAL_GIT_SRV_YAP_DEVOPS,
      url: 'git@github.mpi-internal.com:Yapo/nextgen-api.git'
  }
  stage('Compilar NGA') {
    sh label: '', script: """scl enable python33 'virtualenv ./virtualenv && \\
                    sudo yum install -y libffi libffi-devel &&\\
					          ./virtualenv/bin/pip install pip==10.0.1 && \\
                    ./virtualenv/bin/pip install six packaging appdirs && \\
                    ./virtualenv/bin/pip install --upgrade pip setuptools==19.2 && \\
                    . bootstrap.sh && sh ./rpm/create-rpm.sh'"""

  }
  stage('Asignar Variables') {
    BRANCH = sh(returnStdout: true, script: 'ls rpm/nextgen/x86_64/blocket-nextgen-api-venv-*  | awk -F \'-\' \'{ print $6 }\' | awk -F \'.\' \'{ print $1 }\'')
    BRANCH = BRANCH.replaceAll(~/\n/, "")
    VERSION = sh(returnStdout: true, script: 'ls rpm/nextgen/x86_64/blocket-nextgen-api-venv-*  | awk -F \'-\' \'{ print $5 }\'')
    VERSION = VERSION.replaceAll(~/\n/, "")
    VERSIONCOMMITNGA = "${VERSION}-${BRANCH}"
    TGZFILE = "${VERSION}-${BRANCH}.tgz"

  }
  stage('Comprimir paquetes') {
    echo "${BRANCH}"
    echo "${VERSION}"
    echo "${TGZFILE}"
    sh "tar cvzf ${TGZFILE} -C rpm/nextgen/x86_64 blocket-nextgen-api-venv-${VERSION}-${BRANCH}.x86_64.rpm blocket-nextgen-api-${VERSION}-${BRANCH}.x86_64.rpm blocket-nextgen-conf-${VERSION}-${BRANCH}.x86_64.rpm"
  }
  stage('Copiar a repo') {
    if (v2 == 'true') {
      withCredentials(bindings: [
        sshUserPrivateKey(
          credentialsId: CREDENTIAL_JENKINS_SSH,
          keyFileVariable: 'SSH_KEY_PATH',
          usernameVariable: 'SSH_KEY_USER'
        )
      ]) {
        sh "scp -o StrictHostKeyChecking=no -i ${SSH_KEY_PATH} ${TGZFILE} ${SSH_KEY_USER}@${ip_repo}:/tmp/"
        sh "ssh -o StrictHostKeyChecking=no -i ${SSH_KEY_PATH} ${SSH_KEY_USER}@${ip_repo} 'rm -f /tmp/NGA_packages.tgz && ln -s /tmp/${TGZFILE} /tmp/NGA_packages.tgz'"
      }
    } else {
      sh "scp -o StrictHostKeyChecking=no ${TGZFILE} root@" + ip_repo + ":/tmp/"
      sh "ssh -o StrictHostKeyChecking=no root@" + ip_repo + " \"rm -f /tmp/NGA_packages.tgz && ln -s /tmp/${TGZFILE} /tmp/NGA_packages.tgz\""
    }
  }
  stage('Actualizar Repo') {
    if (v2 == 'true') {
      withCredentials(bindings: [
        sshUserPrivateKey(
          credentialsId: CREDENTIAL_JENKINS_SSH,
          keyFileVariable: 'SSH_KEY_PATH',
          usernameVariable: 'SSH_KEY_USER'
        )
      ]) {
        sh "ssh -o StrictHostKeyChecking=no -i ${SSH_KEY_PATH} ${SSH_KEY_USER}@${ip_repo} 'tar -xvzf /tmp/NGA_packages.tgz -C ${REPODIR}/x86_64'"
        sh "ssh -o StrictHostKeyChecking=no -i ${SSH_KEY_PATH} ${SSH_KEY_USER}@${ip_repo} 'chown -R root.root ${REPODIR}'"
      }
    } else {
      sh 'ssh root@' + ip_repo + ' "sudo tar -xvzf /tmp/NGA_packages.tgz -C ' + REPODIR + '/x86_64"'
      sh 'ssh root@' + ip_repo + ' "sudo chown -R root.root ' + REPODIR + '"'
      //sh 'ssh root@'+ip_repo+' "sudo /usr/bin/createrepo --update --workers 12 '+REPODIR+'"'
    }
  }
  stage('send to chef') {
    build job: 'upload_to_chef',
      parameters: [
        string(name: 'nga', value: VERSION),
        string(name: 'ambiente_nombre', value: ambiente_nombre),
        string(name: 'repo', value: ip_repo),
        string(name: 'v2', value: v2)
      ],
      wait: true
  }
}


