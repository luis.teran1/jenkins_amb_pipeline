def TGZFILE = ""
def VERSIONCOMMIT = ""
def VERSIONCOMMITNGA = ""
def REPODIR = "/opt/repo/yapo"
CREDENTIAL_GIT_SRV_YAP_DEVOPS='9c4e41a9-6dbf-426e-bd2a-f1d02d2fcd69'
CREDENTIAL_JENKINS_SSH='0465c64d-ce73-4808-be49-8133be8e5304'

node('docker') {
  stage('download Sitio git') {
    sh label: '', script: 'whoami'
    git branch: '${site_branch}',
      credentialsId: CREDENTIAL_GIT_SRV_YAP_DEVOPS,
      url: 'git@github.mpi-internal.com:Yapo/Yapo.cl.git'
  }
  stage('Compilar Sitio') {
    sh label: '', script: 'make rpm-build-prod'
  }
  stage('Asignar Variables') {
    TGZFILE = sh(returnStdout: true, script: 'grep "^Version" rpm/blocket.spec | cut -d \' \' -f 2')
    TGZFILE = TGZFILE.replaceAll(~/\n/, "")
    echo "${TGZFILE}"
    TGZFILE = "${TGZFILE}.tgz"
    sh 'pwd'
    VERSIONCOMMIT = sh(returnStdout: true, script: 'ls rpm/x86_64/ -l | awk \'NR==2{print $9}\' | awk \'{split($0,a,"-"); print a[3]"-"a[4];}\' | awk \'{split($0,a,".x86"); print a[1]}\'')
    VERSIONCOMMIT = VERSIONCOMMIT.replaceAll(~/\n/, "")
    echo "${VERSIONCOMMIT}"
  }
  stage('Comprimir paquetes') {
    echo "${TGZFILE}"
    sh "tar cvzf ${TGZFILE} -C rpm/ noarch x86_64"
  }
  stage('Copiar a repo') {
    if (v2 == 'true') {
      withCredentials(bindings: [
        sshUserPrivateKey(
          credentialsId: CREDENTIAL_JENKINS_SSH,
          keyFileVariable: 'SSH_KEY_PATH',
          usernameVariable: 'SSH_KEY_USER'
        )
      ]) {
        sh "scp -o StrictHostKeyChecking=no -i ${SSH_KEY_PATH} ${TGZFILE} ${SSH_KEY_USER}@${ip_repo}:/tmp/"
        sh "ssh -o StrictHostKeyChecking=no -i ${SSH_KEY_PATH} ${SSH_KEY_USER}@${ip_repo} 'rm -f /tmp/site_packages.tgz && ln -s /tmp/${TGZFILE} /tmp/site_packages.tgz'"
      }
    } else {
      sh "scp -o StrictHostKeyChecking=no ${TGZFILE} root@" + ip_repo + ":/tmp/"
      sh "ssh -o StrictHostKeyChecking=no root@" + ip_repo + " \"rm -f /tmp/site_packages.tgz && ln -s /tmp/${TGZFILE} /tmp/site_packages.tgz\""
    }
  }
  stage('Step Actualizacion de Paquetes en Repo') {
    if (v2 == 'true') {
      withCredentials(bindings: [
        sshUserPrivateKey(
          credentialsId: CREDENTIAL_JENKINS_SSH,
          keyFileVariable: 'SSH_KEY_PATH',
          usernameVariable: 'SSH_KEY_USER'
        )
      ]) {
        sh "ssh -o StrictHostKeyChecking=no -i ${SSH_KEY_PATH} ${SSH_KEY_USER}@${ip_repo} 'tar -xvzf /tmp/site_packages.tgz -C /opt/repo/yapo'"
        sh "ssh -o StrictHostKeyChecking=no -i ${SSH_KEY_PATH} ${SSH_KEY_USER}@${ip_repo} 'chown -R root.root /opt/repo/yapo'"
      }
    } else {
      sh 'ssh root@' + ip_repo + '  "sudo tar -xvzf /tmp/site_packages.tgz -C /opt/repo/yapo"'
      sh 'ssh root@' + ip_repo + '  "sudo chown -R root.root /opt/repo/yapo"'
      //sh 'ssh root@'+ip_repo+'  "sudo /usr/bin/createrepo --update --workers 12 /opt/repo/yapo"'
    }
  }
  stage('send to chef') {
    build job: 'upload_to_chef',
      parameters: [
        string(name: 'sitio', value: VERSIONCOMMIT),
        string(name: 'ambiente_nombre', value: ambiente_nombre),
        string(name: 'repo', value: ip_repo),
        string(name: 'v2', value: v2)],
      wait: true
  }
}

