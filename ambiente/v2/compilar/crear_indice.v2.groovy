node('docker_all') {
  ENVIRONMENT = ambiente
  retry(count: 5) {
    withCredentials(bindings: [
      sshUserPrivateKey(
        credentialsId: '0465c64d-ce73-4808-be49-8133be8e5304',
        keyFileVariable: 'SSH_KEY_PATH',
        usernameVariable: 'SSH_KEY_USER'
      ),
      sshUserPrivateKey(
        credentialsId: '9289af24-e2f2-4b4d-a4ae-5ab7dbabeb8a',
        keyFileVariable: 'KNIFE_KEY_PATH',
        usernameVariable: 'KNIFE_NODE_NAME'
      )]
    ) {
      CHEF_SERVER_URL = "https://chef-server.pro.yapo.cl/organizations/yapo"

      KNIFE_COMMON_OPTIONS = ""
      KNIFE_COMMON_OPTIONS += " --config-option client_key=${KNIFE_KEY_PATH}"
      KNIFE_COMMON_OPTIONS += " --config-option node_name=${KNIFE_NODE_NAME}"
      KNIFE_COMMON_OPTIONS += " --config-option chef_server_url=${CHEF_SERVER_URL}"
      KNIFE_SSH_OPTIONS = "-x ${SSH_KEY_USER} -i ${SSH_KEY_PATH}"

      stage('aceptar llave') {
        query = "(roles:cronjobs OR roles:jenkins_cronjob)"
        command = "sudo -u bsearch ssh -t -oStrictHostKeyChecking=no bsearch@${search} 'hostname'"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
      }

      if (limpiar_directorio == 'true') {
        stage('limpiar directorio') {
          def query = "(roles:cronjobs OR roles:jenkins_cronjob)"
          def command = 'sudo -u bsearch rm -rf /var/opt/bsearch/index/'
          sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
        }
      }
      stage('Reindex') {
        def query = "(roles:cronjobs OR roles:jenkins_cronjob)"
        def command = "sudo -u bsearch /opt/blocket/bin/index_ctl reindex ${tipo_search}"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
      }
      stage('Distribute') {
        def query = "(roles:cronjobs OR roles:jenkins_cronjob)"
        def command = "sudo -u bsearch /opt/blocket/bin/index_ctl distribute_full ${tipo_search} all"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
      }
      stage('Start Search') {
        def query = "(roles:searchdev)"
        def command = "/etc/init.d/${tipo_search} restart"
        sh "knife ssh --no-host-key-verify ${KNIFE_COMMON_OPTIONS} '${query} AND chef_environment:${ENVIRONMENT}' -a ipaddress ${KNIFE_SSH_OPTIONS} '${command}'"
      }
    }
  }
}
