properties([
  parameters([
    string(defaultValue: '', description: '', name: 'nga_branch', trim: false),
    string(defaultValue: '10.45.1.201', description: '', name: 'ip_repo', trim: false),
    choice(choices: ['stg'], description: '', name: 'ambiente_nombre')
  ])
])

def TGZFILE = ""
def VERSIONCOMMIT = ""
def REPODIR = "/opt/repo/yapo"

environment {
  def VERSIONCOMMITNGA = ""
}

node('docker') {
  stage('download NGA git') {
    git branch: '${nga_branch}',
      credentialsId: 'e468eb3f-29ca-4197-8c6d-d5305aac78d1',
      url: 'git@github.mpi-internal.com:Yapo/nextgen-api.git'
  }
  stage('Compilar NGA') {
    //fix centos6 repo
    sh "/bin/mv /etc/yum.repos.d/CentOS*.repo /tmp/"
    sh "/bin/sed 's/^mirrorlist/\\#mirrorlist/; s/\\#baseurl/baseurl/; s/mirror\\.centos\\.org/200\\.29\\.173\\.147/' /tmp/CentOS-Base.repo > /etc/yum.repos.d/CentOS.repo"
    sh "rm -f /etc/yum.repos.d/CentOS-*.repo"
    sh "cat /etc/yum.repos.d/CentOS.repo"
    sh "sudo yum upgrade -y curl"
    sh "sudo yum upgrade -y openssl"

    sh label: '', script: '''scl enable python33 \'virtualenv ./virtualenv && \\
                    sudo yum install -y libffi libffi-devel && \\
                    curl \"https://bootstrap.pypa.io/pip/3.3/get-pip.py\" -o \"get-pip.py\" && \\
                    ./virtualenv/bin/python get-pip.py --no-wheel "pip == 10.0.1" &&\\
                    ./virtualenv/bin/pip install -v six packaging appdirs && \\
                    ./virtualenv/bin/pip install -v setuptools==19.2 && \\
                    . bootstrap.sh && sh ./rpm/create-rpm.sh\''''

  }
  stage('Asignar Variables') {
    BRANCH = sh(returnStdout: true, script: 'ls rpm/nextgen/x86_64/blocket-nextgen-api-venv-*  | awk -F \'-\' \'{ print $6 }\' | awk -F \'.\' \'{ print $1 }\'')
    BRANCH = BRANCH.replaceAll(~/\n/, "")
    VERSION = sh(returnStdout: true, script: 'ls rpm/nextgen/x86_64/blocket-nextgen-api-venv-*  | awk -F \'-\' \'{ print $5 }\'')
    VERSION = VERSION.replaceAll(~/\n/, "")
    env.VERSIONCOMMITNGA = "${VERSION}-${BRANCH}"
    TGZFILE = "${VERSION}-${BRANCH}.tgz"
  }
  stage('Comprimir paquetes') {
    echo "${BRANCH}"
    echo "${VERSION}"
    echo "${TGZFILE}"
    sh "tar cvzf ${TGZFILE} -C rpm/nextgen/x86_64 blocket-nextgen-api-venv-${VERSION}-${BRANCH}.x86_64.rpm blocket-nextgen-api-${VERSION}-${BRANCH}.x86_64.rpm blocket-nextgen-conf-${VERSION}-${BRANCH}.x86_64.rpm"
  }
  stage('Copiar a repo') {
    sh "scp -o StrictHostKeyChecking=no ${TGZFILE} root@" + ip_repo + ":/tmp/"
    sh "ssh -o StrictHostKeyChecking=no root@" + ip_repo + " \"rm -f /tmp/NGA_packages.tgz && ln -s /tmp/${TGZFILE} /tmp/NGA_packages.tgz\""
  }
  stage('Actualizar Repo') {
    sh 'ssh root@' + ip_repo + ' "sudo tar -xvzf /tmp/NGA_packages.tgz -C ' + REPODIR + '/x86_64"'
    sh 'ssh root@' + ip_repo + ' "sudo chown -R root.root ' + REPODIR + '"'
  }
}


